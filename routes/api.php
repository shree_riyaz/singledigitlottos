<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api']], function () {
    /*
   |--------------------------------------------------------------------------
   | Device Controller
   |--------------------------------------------------------------------------
   */
    Route::post('devices/processMobileDevice', 'Api\DeviceController@process');
    Route::post('devices/toggleNotification', 'Api\DeviceController@toggleNotification');
    Route::resource('devices', 'DeviceController');
    /*
    |--------------------------------------------------------------------------
    | User Controller
    |--------------------------------------------------------------------------
    */
    Route::post('logout', 'Api\AuthController@logout');
    Route::get('users/me', 'Api\UserController@getMyAccount');
    Route::put('users/enableNotifications', 'Api\UserController@enableNotifications');
    Route::post('users/update', 'Api\UserController@update');
    Route::post('users/delete', 'Api\UserController@destroy');

    Route::get('tournament', 'Api\ApiController@getTournament');
    Route::post('userlotto/create', 'Api\UserLottoController@createUserLotto');
    Route::get('userlotto', 'Api\UserLottoController@getUserLotto');


    Route::get('user/{id}', 'Api\ApiController@userData');
    Route::get('tournament/winner', 'Api\ApiController@tournamentWinner');
    Route::post('tournament/register', 'Api\ApiController@userTournamentRegisteration');
});
/*
|--------------------------------------------------------------------------
| Authenticate...
|--------------------------------------------------------------------------
*/
Route::get('/v1/lotto/result', 'Api\ApiController@addResult');
Route::get('/v1/lotto/getresult/{type}', 'Api\ApiController@getResult');
Route::post('/login', 'Api\AuthController@validateUser');
Route::post('{provider?}/login', 'Api\AuthController@loginUseProvider');
/*
|--------------------------------------------------------------------------
| Lotto...
|--------------------------------------------------------------------------
*/
Route::get('/v1/history/{type}', 'Api\ApiController@history');
Route::get('lotto/types', 'Api\LottoController@getLotto');
Route::get('/v1/rankings', 'Api\ApiController@getRanking');