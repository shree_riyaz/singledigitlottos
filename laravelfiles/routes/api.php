<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api']], function () {
    /*
   |--------------------------------------------------------------------------
   | Device Controller
   |--------------------------------------------------------------------------
   */
    Route::post('devices/processMobileDevice', 'Api\DeviceController@process');
    Route::post('devices/toggleNotification', 'Api\DeviceController@toggleNotification');
    Route::resource('devices', 'DeviceController');
    /*
    |--------------------------------------------------------------------------
    | User Controller
    |--------------------------------------------------------------------------
    */
    Route::post('logout', 'Api\AuthController@logout');
    Route::get('users/me', 'Api\UserController@getMyAccount');
    Route::put('users/enableNotifications', 'Api\UserController@enableNotifications');
    Route::post('users/update', 'Api\UserController@update');
    Route::post('users/delete', 'Api\UserController@destroy');
    Route::get('tournament', 'Api\ApiController@getTournament');
    Route::post('userlotto/create', 'Api\UserLottoController@createUserLotto');
    Route::get('userlotto', 'Api\UserLottoController@getUserLotto');
    Route::get('tournament/winner', 'Api\ApiController@tournamentWinner');
    Route::post('tournament/register', 'Api\ApiController@userTournamentRegisteration');
    Route::post('reference', 'Api\ApiController@applyReferCode');
    Route::get('notification', 'Api\ApiController@getNotification');
    Route::post('notification/seen', 'Api\ApiController@notificationSeen');
    Route::post('user/account/reset', 'Api\ApiController@resetMainAccount');
    Route::get('user/tournament', 'Api\ApiController@userTournament');
    Route::get('user/history/{type}/{method}', 'Api\ApiController@userHistory');



});


/*
|--------------------------------------------------------------------------
| Authenticate...
|--------------------------------------------------------------------------
*/
Route::get('/v1/result', 'Api\ApiController@addResult');
Route::get('/v1/getresult/{type}', 'Api\ApiController@getResult');
Route::post('/login', 'Api\AuthController@validateUser');
Route::post('{provider?}/login', 'Api\AuthController@loginUseProvider');
Route::get('/v1/gettournament/{id}', 'Api\ApiController@getTournamentById');
Route::get('/v1/user/{id}', 'Api\ApiController@userData');

/*
|--------------------------------------------------------------------------
| Lotto...
|--------------------------------------------------------------------------
*/
Route::get('/v1/history/{type}', 'Api\ApiController@history');
Route::get('lotto/types', 'Api\LottoController@getLotto');
Route::get('/v1/rankings', 'Api\ApiController@getRanking');
Route::get('/v1/cashrankings', 'Api\ApiController@getRankingbycash');
Route::get('/v1/getaccounts', 'Api\ApiController@getAccounts');
Route::get('/v1/getrankings/{id}', 'Api\ApiController@getAccountrankings');
Route::get('/v1/tournament/guest', 'Api\ApiController@getTournamentForGuest');
