<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserNotificationTable extends Migration
{
    public function up()
    {
        Schema::create('user_notifications',function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('notification_id')->nullable();
            $table->boolean('seen')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('user_notifications');
    }
}
