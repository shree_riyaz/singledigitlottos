<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserLotto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_lotto', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('lotto_id')->nullable();
            $table->string('draw_number')->nullable();
            $table->integer('ticket_purchased')->nullable();
            $table->integer('total_amount')->nullable();
            $table->integer('is_tournament')->nullable();
            $table->integer('tournament_id')->nullable();
            $table->enum('lotto_type', ['binary', 'dice', 'decimal'])->nullable();
            $table->enum('method', ['daily', 'instant'])->nullable();
            $table->enum('result', ['win', 'lost','pending'])->nullable();
            $table->integer('result_number')->nullable();
            $table->float('prize')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_lotto');
    }
}
