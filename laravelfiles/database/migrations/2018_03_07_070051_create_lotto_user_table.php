<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLottoUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotto_user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('phone_number')->nullable();
            $table->string('user_name')->nullable();
            $table->string('password');
            $table->dateTime('last_login');
            $table->dateTime('dob');
            $table->integer('no_of_login');
            $table->string('country');
            $table->string('timezone');
            $table->char('gender', 1);
            $table->string('reffered_by');
            $table->string('account_verification_string');
            $table->integer('retry_attempts');
            $table->string('IP');
            $table->char('account_type',1);
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lotto_user');
    }
}
