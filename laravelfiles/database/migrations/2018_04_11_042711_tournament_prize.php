<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TournamentPrize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournament_prize',function(Blueprint $table){
            $table->increments('id');
            $table->integer('tournament_id');
            $table->string('prize_name')->nullable();
            $table->integer('min_points')->nullable();
                $table->integer('max_points')->nullable();
            $table->integer('prize_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournament_prize');
    }
}
