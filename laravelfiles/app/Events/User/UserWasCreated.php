<?php
namespace App\Events\User;

use App\Events\Event;
use App\Model\User;

class UserWasCreated extends Event
{
    public $user;
    public $data;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $data=[])
    {
        $this->user = $user;
        $this->data = $data;
    }
}
