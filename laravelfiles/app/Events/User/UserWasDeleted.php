<?php
namespace App\Events\User;

use App\Events\Event;
use App\Model\User;

class UserWasDeleted extends Event
{
    public $user;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
