<?php

namespace App\Http\Controllers;

use App\Model\Lotto;
use App\Model\LottoResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Model\User;
use App\Model\Role;

class ApiController extends Controller
{
    //
    public function validateUser(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()){
            $error = 'Opps, Something went wrong';
            if(!empty($validator->errors()->getMessages())){
                foreach ($validator->errors()->getMessages() as $key=>$value){
                    $error = isset($value[0]) ? $value[0] : 'Opps, Something went wrong';
                }
            }
            return response()->json([
                'status'=>FALSE,
                'error'=>$error,
            ]);
        }

        $user = User::where('email',$request->email)->first();
        if($user != null){


                if ($user) {
                    $token = $user->createToken($user->name)->accessToken;
                    return  $response = ['success' => 1, 'data' => $user->toArray(), 'token' => $token];
                } else {
                    return $response = ['success' => 0, 'data' => 'No record found'];
                }

        }else{
            return $response = ['success' => 0, 'data' => 'No record found'];
        }

    }
    public function addResult()
    {
        $hour = date('H:00:00');
        $lottos =   Lotto::where('drow_time',$hour)->get();

        foreach($lottos as $lotto)
        {
            if($lotto->type=='decimal')
            {
                $rand = rand(0,9);
            }
            elseif($lotto->type=='dice')
            {
                $rand = rand(0,6);
            }
            else
            {
                $rand = rand(0,1);
            }

            $resultData['lottoId'] = $lotto->id;
            $resultData['result'] = $rand;
            LottoResult::create($resultData);
        }
    }

}
