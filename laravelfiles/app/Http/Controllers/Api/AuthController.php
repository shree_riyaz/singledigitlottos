<?php

namespace App\Http\Controllers\Api;

use DB;
use Hash;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Model\User\Provider;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\DeviceRepositoryInterface;
use App\Model\UserLotto;
class AuthController extends RestfulController
{
    protected $userRepository;
    protected $deviceRepository;

    /**
     * Constructor
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        DeviceRepositoryInterface $deviceRepository
    ){
        $this->userRepository = $userRepository;
        $this->deviceRepository = $deviceRepository;
    }

    public function validateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'user_name' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $error = 'Opps, Something went wrong';
            if (!empty($validator->errors()->getMessages())) {
                foreach ($validator->errors()->getMessages() as $key => $value) {
                    $error = isset($value[0]) ? $value[0] : 'Opps, Something went wrong';
                }
            }
            return response()->json([
                'status' => FALSE,
                'error' => $error,
            ]);
        }

        $user = User::where('email', $request->email)->first();
        if ($user != null) {
            if ($user) {
                $token = $user->createToken($user->user_name)->accessToken;
                return $response = ['success' => 1, 'data' => $user->toArray(), 'token' => $token];
            } else {
                return $response = ['success' => 0, 'data' => 'No record found'];
            }

        } else {
            return $response = ['success' => 0, 'data' => 'No record found'];
        }

    }


    /**
     * Auth login use provider.
     *
     * @param  Request  $request
     * @return Response
     */
    public function loginUseProvider(Request $request, $provider = 'facebook') {
        // validate provider is valid
        if ($provider != 'facebook' && $provider !='google') {
            return $this->_error('Provider must be facebook or google');
        }

        // validate input request before create new user.
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'user_name'  => 'required',
            'id'    => 'required'
        ]);

        if ($validator->fails()) {
            $error = 'Opps, Something went wrong';
            if (!empty($validator->errors()->getMessages())) {
                foreach ($validator->errors()->getMessages() as $key => $value) {
                    $error = isset($value[0]) ? $value[0] : 'Opps, Something went wrong';
                }
            }
            return response()->json([
                'status' => FALSE,
                'error' => $error,
            ]);
        }
        $email = $request->get('email');
        $provider_user_id = $request->get('id');

        $socialAccount = Provider::whereProvider($provider)
            ->whereProviderUserId($provider_user_id)
            ->first();

        if ($socialAccount) {
            $user = $socialAccount->user;
            $this->userRepository->incrementLogNum($user->id);

            // creating a token without scopes...
            $token = $user->createToken($user->user_name)->accessToken;
            //$token = $user->createToken(self::TOKEN_NAME)->accessToken;

            $this->userRepository->prepareUserData($user, $provider);


            $user->access_token = $token;

            return $this->response($user);
        } else {
            try{
                $found = $this->userRepository->findByEmail($email);
                if(!$found){
                    date_default_timezone_set("Asia/Kolkata");

                    #create new user
                    $user = new User();
                    if($request->get('firstname'))
                        $user->firstname = ($request->get('firstname')?$request->get('firstname'):'').' '.($request->get('lastname')?$request->get('lastname'):'');
                    if($request->get('lastname'))
                        $user->lastname = $request->get('lastname')?$request->get('lastname'):'';
                    $user->email = $email?$email:'';
                    if($request->get('user_name'))
                        $user->user_name  = $request->get('user_name')?$request->get('user_name'):'';
                    if($request->get('last_login'))
                        $user->last_login  = $request->get('last_login')?$request->get('last_login'):'';
                    if($request->get('country'))
                        $user->country  = $request->get('country')?$request->get('country'):'';
                    if($request->get('timezone'))
                        $user->timezone  = $request->get('timezone')?$request->get('timezone'):'';
                    if($request->get('IP'))
                        $user->IP  = $request->get('IP')?$request->get('IP'):'';
                    $user->main_balance  = env('TOURNAMENT_PRE_FOUND');
                    $user->save();
                    $details =  array('user_id' => $user->id,'prize' => 1000, 'initial_balance' => 0, 'final_balance' => env('TOURNAMENT_PRE_FOUND'), 'lotto_id' => 0, 'result' => 'win', 'description' => 'Sign up bonus');
                    $userLotto = UserLotto::create($details);

                }else{
                    $user = $found;
                }

                $socialAccount = new Provider([
                    'user_id' => $user->id,
                    'provider_user_id' => $provider_user_id,
                    'provider' => $provider
                ]);
                $socialAccount->save();
                $this->userRepository->incrementLogNum($user->id);
                $token = $user->createToken($user->user_name)->accessToken;
                $this->userRepository->prepareUserData($user, $provider);
                $user->access_token = $token;
                return $this->response($user);
            } catch (\Exception $ex) {
                return $this->_critical($ex);
            }
        }
    }

    /**
     * Logout action
     *
     * @param  Request  $request
     * @return Response
     */
    public function logout(Request $request){
        $validator = Validator::make($request->all(), [
            'device_id'    => 'required',
            'user_id'    => 'required'
        ]);
        if ($validator->fails()) {
            $error = 'Opps, Something went wrong';
            if (!empty($validator->errors()->getMessages())) {
                foreach ($validator->errors()->getMessages() as $key => $value) {
                    $error = isset($value[0]) ? $value[0] : 'Opps, Something went wrong';
                }
            }
            return response()->json([
                'status' => FALSE,
                'error' => $error,
            ]);
        }
        $device_id = $request->get('device_id');
        $user_id   = $request->user()->id;

        try{
            $this->deviceRepository->setDeviceIsInActive($user_id, $device_id);
            return $this->setMessage('success')->response();
        } catch (\Exception $ex) {
            return $this->_error($ex->getMessage());
        }
    }

}
