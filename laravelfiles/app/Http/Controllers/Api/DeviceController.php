<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\DeviceRepositoryInterface;

class DeviceController extends RestfulController
{
    protected $deviceRepository;
    
    /**
     * Constructor
     */
    public function __construct(DeviceRepositoryInterface $deviceRepository){
        $this->deviceRepository = $deviceRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        
        return $this->response();
    }
    
    /**
     * Process the device token.
     *
     * @return Response
     */
    public function process(Request $request){
        $this->validate($request, [
            'device_id'    => 'required',
            'device_token' => 'required',
            'device_os'    => 'required|integer'
        ]);
        
        $user_id      = $request->user()->id;
        $device_id    = $request->get('device_id');
        $device_token = $request->get('device_token');
        $device_os    = $request->get('device_os');
        $this->deviceRepository->process($user_id, $device_id, $device_token, $device_os);
        
        return $this->_success('Device token updated');
    }
    
    /**
     * Toggle notification.
     *
     * @return Response
     */
    public function toggleNotification(Request $request){
        return $this->response();
    }
}
