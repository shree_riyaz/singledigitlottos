<?php
namespace App\Http\Controllers\api;
use App\Model\Notifications;
use App\Model\Tournament;
use App\Model\TournamentUser;
use App\Model\User;
use App\Model\UserLotto;
use App\Model\UserNotifications;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\RestfulController;
use App\Model\LottoResult;
use App\Model\Lotto;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApiController extends RestfulController
{
    #to get tournament details by tournament id
    public function getTournamentById(Request $request)
    {


        $id = $request->id;
        $tournaments = Tournament::where('status', '1')->where('id', $id)->get();
        if ($tournaments) {
            $response['status'] = 'success';
            $response['data'] = $tournaments;
        } else {
            $response['status'] = 'success';
            $response['data'] = array();
        }
        return response()->json($response);
    }
    #to get all tournament names and ids
    public function getTournament(Request $request)
    {

        date_default_timezone_set("Asia/Kolkata");

        $tournaments = Tournament::with(['prizes'])->where('status', '1')->orderBy('start_time','DESC')->get();
//         dd($tournaments);
        if (count($tournaments)) {
            foreach ($tournaments as $tournament) {

                if (((int)strtotime(date('Y-m-d H:i')) >= (int)strtotime($tournament->start_time)) && ((int)strtotime(date('Y-m-d H:i')) <= (int)strtotime($tournament->end_time))) {
                      
                    $tournament->status = 'running';


                } elseif (((int)strtotime(date('Y-m-d H:i')) >= (int)strtotime($tournament->start_time)) && ((int)strtotime(date('Y-m-d H:i')) >= (int)strtotime($tournament->end_time))) {
                    
                    $tournament->status = 'past';
                } else {
                    $tournament->status = 'upcoming';
                }

                $isregistered = TournamentUser::where(['tournament_id' => $tournament->id, 'user_id' => Auth::user()->id])->get();

                if (!$isregistered->isEmpty()) {
                    $tournament->is_registered = 1;
                } else {
                    $tournament->is_registered = 0;
                }
            }
            $total_prize = 0;
            if ($tournament->prizes) {

                foreach ($tournament->prizes as $prize) {
                    $total_prize += $prize->prize_amount;
                }
            }
            $tournament->total_prize = $total_prize;
            $response['status'] = 'success';

            $response['data'] = $tournaments;

        } else {
            $response['status'] = 'success';
            $response['data'] = 'No tournament available';
        }
        return response()->json($response);
    }
    #to create tournament and daily lotto result
    public function addResult()
    {
        date_default_timezone_set("Asia/Kolkata");
//        $hour = date('H:i:00');
        $hour = '17:30:00';
//         $lastDayHour = date('H:i:01');
        $lastDayHour = '17:30:01';
        
        $lottos = Lotto::where('draw_time', $hour)->get();

        if (!$lottos->isEmpty()) {
            foreach ($lottos as $lotto) {
                $LottoResultExt = LottoResult::where('lottoId', $lotto->id)

                    ->whereDate('created_at', '=', date('Y-m-d'))->get();

                if ($LottoResultExt->isEmpty()) {
//                    dd('sdd');

                    if ($lotto->type == 'decimal') {
                        $rand = rand(0, 9);
                    } elseif ($lotto->type == 'dice') {
                        $rand = rand(0, 6);
                    } else {
                        $rand = rand(0, 1);
                    }
                    $resultData['lottoId'] = $lotto->id;
                    $resultData['result'] = $rand;
                    LottoResult::create($resultData);
                    $from_from = date('Y-m-d ', strtotime('yesterday'));//.$lastDayHour;
                    $to_from = date('Y-m-d');

//                    $lottoUsersNotification = UserLotto::whereBetween('created_at', [$from_from . " $lastDayHour", $to_from . $hour])->select('*')->groupBy('user_id')->where('lotto_type',$lotto->type)->get();
//
                    $lottoUsersNotification = UserLotto::whereBetween('created_at', [$from_from . " $lastDayHour", $to_from .' '. $hour])->select('*')->groupBy('user_id')->where('lotto_type',$lotto->type)->get();

                    $notification = "Result declared for " . $lotto->type . " of Date " . date('d-M-Y');
                    // echo "<pre>";
                    // print_r($notification);
                    foreach ($lottoUsersNotification as $lottoUserss) {

                     if($notification)
                        {

                                $userNotificationData['notification'] = $notification;
                                $userNotificationData['user_id'] = $lottoUserss->user_id;
                                UserNotifications::create($userNotificationData);
                        }
                    }

                    $lottoUsers = UserLotto::whereBetween('created_at', [$from_from . " $lastDayHour", $to_from .' '. $hour])->select('*')->where('lotto_type',$lotto->type)->where('result','=','pending')->get();
//                    dd($lottoUsers);
                    //$notificationMsg['notification'] = $notification;
                    //$notificationId = Notifications::create($notificationMsg)->id;
                    foreach ($lottoUsers as $lottoUser) {

                        $userLottoUpdate = UserLotto::whereId($lottoUser->id)->first();
                        //$cart = \GuzzleHttp\json_decode($lottoUser->draw_number);

                        $resultAmount = 0;

                        if ( $lottoUser->draw_number == $rand) {
                            $totalWinningTickets = $lottoUser->ticket_purchased;
                            if ($lotto->type == 'decimal') {
                                $resultAmount = ($totalWinningTickets * env("DECIMAL_PRICE"));
                            } elseif
                            ($lotto->type == 'dice') {
                                $resultAmount = ($totalWinningTickets * env("DICE_PRICE"));
                            } elseif($lotto->type == 'binary') {
                                $resultAmount = ($totalWinningTickets * env("BINARY_PRICE"));
                            }else{
                                $resultAmount = '';
                            }
                        }

                        if ($resultAmount) {
                            $winNotificationMsg = "You won the " . $lotto->type . " Date " . date('d-M-Y');
                            //$winNotification['notification'] = $winNotificationMsg;
                            // $winNotificationId = Notifications::create($winNotification)->id;
                            if ($winNotificationMsg)
                            {
                                $userWinNotificationData['notification'] = $winNotificationMsg;
                                $userWinNotificationData['user_id'] = $lottoUser->user_id;
                                UserNotifications::create($userWinNotificationData);
                            }
                            if ($userLottoUpdate->is_tournament == 0) {
                                $tournament_id  = $is_tournament = 0;
                                $user = User::where('id', $userLottoUpdate->user_id)->first();

                                $initial_balance = $user->main_balance;
                                $user->main_balance += $resultAmount;
                                $user->save();
                            } else {
                                $is_tournament = 1;
                                $tournament_id = $userLottoUpdate->tournament_id;
                                $userTournament = TournamentUser::where('tournament_id',
                                    $userLottoUpdate->tournament_id)->where('user_id',
                                    $userLottoUpdate->user_id)->first();
                                $initial_balance = $userTournament->balance;
                                $userTournament->balance += $resultAmount;
                                $userTournament->save();
                            }

                            $details =  array('user_id' => $userLottoUpdate->user_id, 'initial_balance' => $initial_balance, 'final_balance' => $initial_balance+$resultAmount, 'lotto_id' => 0, 'lotto_type' => $lotto->type, 'result' => 'win', 'tournament_id'=>$tournament_id,'description' => 'Lotto winner prize for draw number '.$rand,'draw_number'=>null,'result'=>'win','prize'=>$resultAmount, 'is_tournament'=>$is_tournament);

                             $userLotto = UserLotto::create($details);

                            //$userLottoUpdate->prize += $resultAmount;
                            $userLottoUpdate->result = 'win';
                            $userLottoUpdate->save();
                            
                        } else {
                            $userLottoUpdate->result = 'lost';
                            $userLottoUpdate->save();
                        }
                    }
                }
            }
            $response = ['status' => 'success', 'data' => 'Result created'];
        } else {
            $response = ['status' => 'fail', 'data' => 'No result sent'];
        }

        $to_from = date('Y-m-d ');
        $tournaments = Tournament::with('prizes')->where('completed', 'like', 0)->where('end_time', $to_from . $hour)->where('status', 1)->get();
//            dd($tournaments);

        if (!$tournaments->isEmpty()) {
            foreach ($tournaments as $tournament) {

                $notification = "Result declared for tournament " . $tournament->name;
                //$notificationMsg['notification'] = $notification;
                //$tournamentNotificationId = Notifications::create($notificationMsg)->id;

                $userRank = [];
                $tournamentId = $tournament->id;
                $tournamentUsers = TournamentUser::where('tournament_id', $tournamentId)->get();

                foreach ($tournamentUsers as $tournamentUser) {
                    if ($notification)
                    {
                        $userTournamentNotificationData['notification'] = $notification;
                        $userTournamentNotificationData['user_id'] = $tournamentUser->user_id;
                        UserNotifications::create($userTournamentNotificationData);
                     }
                    $userRank[$tournamentUser->user_id] = UserLotto::where('tournament_id', $tournamentId)
                        ->where('user_id', $tournamentUser->user_id)->sum('prize');
                }
                arsort($userRank);


                $prizes = ($tournament->prizes)->toArray();
                $is_ranked = $ranking = array();
                for ($i = 0; $i < count($prizes); $i++) {
                    foreach ($userRank as $uid => $amount) {
                        if ($amount >= $prizes[$i]['min_points']) {
                            if (!in_array($i + 1, $ranking) && !in_array($uid, $is_ranked)) {
                                if ($i + 1 == 1) {
                                    $winNotificationMsg = "You won the " . $tournament->name;
                                    //  $winNotification['notification'] = $winNotificationMsg;
                                    //  $winNotificationId = Notifications::create($winNotification)->id;
                                   if($winNotificationMsg) {
                                       $userWinNotificationData['notification'] = $winNotificationMsg;
                                       $userWinNotificationData['user_id'] = $uid;
                                       UserNotifications::create($userWinNotificationData);
                                   }
                                } else {
                                    $winNotificationMsg = "You got " . ($i + 1) . " rank in " . $tournament->name;
                                    // $winNotification['notification'] = $winNotificationMsg;
                                    // $winNotificationId = Notifications::create($winNotification)->id;
                                    if ($winNotificationMsg)
                                    {
                                    $userWinNotificationData['notification'] = $winNotificationMsg;
                                    $userWinNotificationData['user_id'] = $uid;
                                    UserNotifications::create($userWinNotificationData);
                                }
                                }
                                $tuser = TournamentUser::where('user_id', $uid)->where('tournament_id', $tournamentId)->first();
                                $tuser->ranking = $i + 1;
                                $tuser->prize = $prizes[$i]['prize_amount'];
                                $tuser->save();
                                $user = User::where('id', $uid)->first();
                                $user->cash_prize += $prizes[$i]['prize_amount'];
                                $user->save();
                                $ranking[] = $i + 1;
                                $is_ranked[] = $uid;
                            }
                        }
                    }
                }
                $disable_execution = Tournament::where('id', $tournamentId)->first();
                $disable_execution->completed = 1;
                $disable_execution->save();
            }
            $response = ['status' => 'success', 'data' => 'Result created'];
        }
        return response()->json($response);
    }
        
    #to get result of current date and completed tournament
    public function getResult(Request $request, $type = 'binary')
    {
        $LottoResult = LottoResult::whereHas('lotto', function ($query) use ($type) {
            $query->where([['method', 'daily'], ['type', $type]]);
        })->whereDate('created_at', '=', date('Y-m-d'))->select('result', DB::raw("DATE_FORMAT(created_at,'%d %M %Y %h:%i %p') AS date"))->first();

        if (!$LottoResult) {
            $LottoResult = LottoResult::whereHas('lotto', function ($query) use ($type) {
                $query->where([['method', 'daily'], ['type', $type]]);
            })->whereDate('created_at', '=', date('Y-m-d', strtotime('yesterday')))->select('result', DB::raw("DATE_FORMAT(created_at,'%d %M %Y %h:%i %p') AS date"))->first();
        }

        if ($LottoResult) {
            $LottoResult = ['status' => 'success', 'data' => $LottoResult->toArray()];
        }

        if ($LottoResult) {
            $response = (object)$LottoResult;
        } else {
            $data['result'] = 'Pending';
            $data['date'] = 'Pending';
            $response = ['status' => 'success', 'data' => $data];
        }

        return $this->responseJson($response);
    }
    #Get list of users according to their main account points
    public function getRanking()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $users = User::select(DB::raw('@rownum := 0 r'))->select( 'id',DB::raw("firstname AS name"), 'main_balance', DB::raw('@rownum := @rownum + 1 AS rank'))->where('main_balance', '>', 1000)->orderBy('main_balance', 'DESC')->orderBy('created_at', 'ASC')->get();

//        dd($users);

        if ($users) {
            $response = ['status' => 'success', 'data' => $users];
        } else {
            $response = ['status' => 'fail', 'data' => ''];
        }
        return $this->responseJson($response);
    }

    public function getRanking123()
    {
        $rankdata = User::select('id','main_balance','firstname')->where('main_balance','>',1000)->orderBy('main_balance','DESC')->orderBy('created_at','ASC')->get();
//        dd($rankdata[0]['id']);
        $userRanks = [];
        $userRan = [];
        $i=1; $count =0;
        foreach($rankdata as $data) {
                $userRanks['total_user'] = $rankdata->count();
                $userRanks['rank'] = $i;
                $userRanks['name'] = 'Main account';
                $userRanks['firstname'] = $data->firstname;
            $i++;
            $userRan[] = $userRanks;
        }

        if(!count($userRanks)>0)
        {
            $userRanks['total_user'] = $rankdata->count();
            $userRanks['rank'] = 0;
            $userRanks['name'] = 'Main account';
            $userRan[] = $userRanks;
        }
//        dd($userRan);

        if ($userRan) {
            $response = ['status' => 'success', 'data' => $userRan];
        } else {
            $response = ['status' => 'fail', 'data' => ''];
        }
        return $this->responseJson($response);
    }
    #Get rank basis on win cash
    public function getRankingbycash()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $users = User::select(DB::raw('@rownum := 0 r'))->select('id', DB::raw("CONCAT(firstname,' ',lastname) AS name"), 'cash_prize', DB::raw('@rownum := @rownum + 1 AS rank'))->where('cash_prize','>',0)->orderBy('main_balance', 'DESC')->orderBy('created_at', 'ASC')->get();
        if ($users) {
            $response = ['status' => 'success', 'data' => $users];
        } else {
            $response = ['status' => 'fail', 'data' => ''];
        }
        return $this->responseJson($response);
    }
    #to get result history of all type of game
    public function history(Request $request, $type = 'binary')

    {
        $LottoResult = LottoResult::whereHas('lotto', function ($query) use ($type) {
            $query->whereType($type)->whereMethod('daily');
        })->select('result', 'id', DB::raw("DATE_FORMAT(created_at,'%d %M %Y') AS date"), DB::raw("DATE_FORMAT(created_at,'%a') AS day"), DB::raw("DATE_FORMAT(created_at,'%d %M %Y') AS date"), DB::raw("DATE_FORMAT(created_at,'%U') AS week"), DB::raw("DATE_FORMAT(created_at,'%d') AS monthday"), DB::raw("DATE_FORMAT(created_at,'%m') AS month"), DB::raw("DATE_FORMAT(created_at,'%Y') AS year"))->get();

        if (!$LottoResult->isEmpty()) {
            // $response = (object)  array('status' => 'success', 'data' =>  $LottoResult->toArray());
            $response = $LottoResult->toArray();
        } else {
            $response = array('status' => 'fail', 'data' => 'no data available');
        }
        return $this->response($response);

    }

    #to get user data by user id
    public function userData(Request $request, $id = null)
    {
        $userId = $id;
        $user = User::where('id',$id)->get();

        if ($user->isEmpty()) {
            return $this->_error('Not found');
        }

        $rankdata = User::select('id','main_balance')->where('main_balance','>',1000)->orderBy('main_balance','DESC')->orderBy('created_at','ASC')->get();
        $userRanks = [];
        $i=1; $count =0;
        foreach($rankdata as $data) {
            if($data->id==$id) {
                $userRanks['account'][$count]['total_user'] = $rankdata->count();
                $userRanks['account'][$count]['rank'] = $i;
                $userRanks['account'][$count]['name'] = 'Main account';
            }
            $i++;
        }
        if(!count($userRanks)>0)
        {
            $userRanks['account'][$count]['total_user'] = $rankdata->count();
            $userRanks['account'][$count]['rank'] = 0;
            $userRanks['account'][$count]['name'] = 'Main account';
        }
        // echo '<pre>';  print_r($userRanks);
        $rankdata = TournamentUser::with(['tournament','user'])->where('user_id',$id)->get();
        $count =1;
        foreach($rankdata as $udata)
        {
            if(isset($udata->tournament->name))
            {
                $userRanks['account'][$count]['rank'] = $udata->ranking?:0;
                $userRanks['account'][$count]['name'] = 'Tournament - '.$udata->tournament->name?:'N/a';
                $userRanks['account'][$count]['total_user'] =
                    TournamentUser::where('tournament_id',$udata->tournament_id)->count()?:0;
                $count++;
            }
        }
        $userData =$user->toArray();
        $user = array_merge($userData[0],$userRanks);
        if (count($user)) {
            $response = ['status' => 'success', 'data' =>$user ];
        } else {
            $response = ['status' => 'fail', 'data' => 'no user found'];
        }
        return response()->json($response);
    }
    # to findout the winners of tournament
    public function tournamentWinner()
    {
        $tournaments = Tournament::where('status', '1')->get();
    }
    # functionality to apply reference cde
    public function applyReferCode(Request $request)
    {
        date_default_timezone_set("Asia/Kolkata");
        $referenceCode = $request->get('ref');

        if (Auth::user()->no_of_login <= 1) {
            $referalId = Auth::user()->id;

            $refereeId = User::where('email', $referenceCode)->pluck('id')->first();
//            dd($refereeId);

            if ($refereeId) {
                $referalData['user_id'] = $referalId;
                $referalData['reffered_by'] = $refereeId;
                $user = User::whereId(Auth::user()->id)->first();
                $user->reffered_by = $refereeId;
                $user->main_balance += env('referal_points');
                $user->save();
                $refereeData = User::whereId($refereeId)->first();
                $refree_initial_balance = $refereeData->main_balance;
                $refereeData->main_balance += env('referal_points');
//                $refree_final_balance = $refereeData->main_balance += env('referal_points');

                $refereeData->save();

                $initial_balance = Auth::user()->main_balance;
                $final_balance = Auth::user()->main_balance +=env('referal_points');
                $referred_to = $refereeData->email;


                // logged in user bonus
                $details =  array('user_id' => Auth::user()->id, 'initial_balance' => $initial_balance, 'final_balance' => $final_balance, 'lotto_id' => 0, 'result' => 'win','prize' => env('referal_points'),'description' => 'Reference bonus Referred to '.$referred_to);
                $userLotto = UserLotto::create($details);

                // Referral user bonus
                $refree_final_balance = $refereeData->main_balance;
                $referred_by = Auth::user()->email;

                $details =  array('user_id' =>$refereeData->id, 'initial_balance' => $refree_initial_balance, 'final_balance' => $refree_final_balance, 'lotto_id' => 0, 'result' => 'win','prize' => env('referal_points'),'description' => 'Reference bonus Referred by '.$referred_by);
                $userLotto = UserLotto::create($details);

                $response['status'] = 'success';
                $response['data'] = 'Reference code added successfully';
            } else {
                $response['status'] = 'fail';
                $response['data'] = 'Reference code is incorrect';
            }
        } else {
            $response['status'] = 'fail';
            $response['data'] = 'Reference code applicable on first login only';
        }
        return response()->json($response);
    }
    #Register for tournament
    public function userTournamentRegisteration(Request $request)
    {
                date_default_timezone_set("Asia/Kolkata");

        $tournament_id = $request->tournament_id;
        $user_id = Auth::user()->id;
        $checkTournament = Tournament::whereId($tournament_id)->pluck('id')->first();
        if (!$checkTournament) {
            $response['status'] = 'fail';
            $response['data'] = 'Tournament not exist';
        } else {
            $checkUser = TournamentUser::whereUserId($user_id)->whereTournamentId($tournament_id)->pluck('id')->first();
            if ($checkUser) {
                $response['status'] = 'fail';
                $response['data'] = 'already registered';
            } else {
                $data['user_id'] = $user_id;
                $data['tournament_id'] = $tournament_id;
                $data['balance'] = env("TOURNAMENT_PRE_FOUND");
                $result = TournamentUser::create($data);


                $details =  array('user_id' => $user_id, 'initial_balance' => 0,'prize' => 1000, 'final_balance' => env('TOURNAMENT_PRE_FOUND'), 'lotto_id' => 0, 'result' => 'win', 'description' => 'Tournament registration bonus','tournament_id'=>$tournament_id,'is_tournament'=>1);
                $userLotto = UserLotto::create($details);


                if ($result->id) {
                    $response['status'] = 'success';
                    $response['data'] = 'Registration successful';
                } else {
                    $response['status'] = 'fail';
                    $response['data'] = 'Registration unsuccessful';
                }
            }
        }

        return response()->json($response);
    }

    # To mark notification seen
    public function notificationSeen()
    {
        $userId = Auth::user()->id;
        $userNotifications = UserNotifications::where('user_id', $userId)->get();
        if (!$userNotifications->isEmpty()) {
            foreach($userNotifications as $userNotification)
            {
                $updateNotification = UserNotifications::where('id', $userNotification->id)->first();
                $updateNotification->seen = 1;
                $updateNotification->save();
            }
            $response = ['status' => 'success', 'data' => 'Notifications are seen now'];
        } else {
            $response = ['status' => 'fail', 'data' => 'Unseen notifications are not available'];
        }

        return response()->json($response);
    }
    # Ro get user account notification
    public function getNotification()
    {
        $userId = Auth::user()->id;
        $notificration = UserNotifications::where('user_id',$userId)->where('seen',0)->get();
        if(!$notificration->isEmpty())
        {
            $response = ['status' => 'success', 'data' => $notificration];
        }
        else
        {
            $response = ['status' => 'fail', 'data' => 'No notification available'];
        }

        return response()->json($response);
    }
    # Reset user main account data
    public function resetMainAccount()
    {
        date_default_timezone_set("Asia/Kolkata");

        $userId = Auth::user()->id;
        $userLotto = UserLotto::where('user_id',$userId)->where('is_tournament',0)->delete();
        $userLotto = UserNotifications::where('user_id',$userId)->delete();
        $userAccount = User::where('id',$userId)->first();
        $userAccount->main_balance = env('TOURNAMENT_PRE_FOUND');
        $userAccount->save();

        $details =  array('user_id' => Auth::user()->id, 'initial_balance' => 0,'prize' => 1000, 'final_balance' => env('TOURNAMENT_PRE_FOUND'), 'lotto_id' => 0, 'result' => 'win', 'description' => 'Reset account bonus', 'method' => '');
        $userLotto = UserLotto::create($details);

        $response = ['status' => 'success', 'data' => 'Account reset successfully'];
        return response()->json($response);
    }

    # list of all user registered tournament

    public function userTournament()
    {
        $userId = Auth::user()->id;
        $tournamentUser = TournamentUser::with('tournament')->has('tournament')->where('user_id',$userId)->get();
        if(!$tournamentUser->isEmpty())
        {
            $response = ['status' => 'success', 'data' => $tournamentUser];
        }
        else
        {
            $response = ['status' => 'fail', 'data' => 'No tournament registered'];
        }
        return response()->json($response);
    }

    # list of all completed tournament of a user
    public function getAccounts()
    {
        date_default_timezone_set("Asia/Kolkata");

        $tournaments = Tournament::whereDate('end_time','<=', date('Y-m-d'))->whereTime('end_time','<=', date('H:i:s'))->select('name','id')->orderBy('name')->where('completed',1)->get();
        if(!$tournaments->isEmpty())
        {
            $response = ['status' => 'success', 'data' => $tournaments];
        }
        else
        {
            $response = ['status' => 'fail', 'data' => 'No accounts available'];
        }
        return response()->json($response);

    }
    # get users according to ranks in tournament
    public function getAccountrankings($id)
    {
        $rankdata = TournamentUser::with(['tournament','user'])->where('tournament_id',$id)->orderBy('ranking','ASC')->orderBy('prize','DESC')->get();
//        $rankdata = TournamentUser::with(['tournament','user'])->where('tournament_id',$id)->orderBy('prize','DESC')->orderBy('ranking','ASC')->get();
//        dd($rankdata);
        $count = 0;
        $userRanks = array();
        if(!$rankdata->isEmpty()){
            foreach($rankdata as $udata)
            {
                if(isset($udata->tournament->name))
                {
                    $userRanks[$count]['id'] = $udata->user->id;
                    $userRanks[$count]['rank'] = $udata->ranking?:0;
                    $userRanks[$count]['name'] = $udata->user->firstname?:'N/a';
                    $userRanks[$count]['prize'] = $udata->prize?:0;
                    $count++;
                }
            }

            $response = ['status' => 'success', 'data' => $userRanks];
        }
        else
        {
            $response = ['status' => 'fail', 'data' => 'No data available'];
        }
        return response()->json($response);

        /*
                DB::statement(DB::raw('set @rownum=0'));
                $users = User::select(DB::raw('@rownum := 0 r'))->select('id', DB::raw("CONCAT(firstname,' ',lastname) AS name"), 'main_balance', DB::raw('@rownum := @rownum + 1 AS rank'))->where('main_balance', '>', 1000)->orderBy('main_balance', 'DESC')->orderBy('created_at', 'ASC')->get();
                if ($users) {
                    $response = ['status' => 'success', 'data' => $users];
                } else {
                    $response = ['status' => 'fail', 'data' => ''];
                }
                return $this->responseJson($response);*/


    }

    public function userHistory($type,$method)
    {
        if($type!='' && $method!='')
        {
            $userId = Auth::user()->id;
            // $lottoHistory =  UserLotto::where('lotto_type','=',$type)->where('method','=',$method)->where('is_tournament',0)->where('user_id',$userId)->get();
                        $lottoHistory =  UserLotto::where('lotto_type','=',$type)->where('draw_number','!=','')->where('method','=',$method)->where('is_tournament',0)->where('user_id',$userId)->get();

            if(!$lottoHistory->isEmpty())
            {
                foreach ($lottoHistory as $lottoHistoryList){
                    if($lottoHistoryList->result == 'pending'){
                        $lottoHistoryList->result_number = 'pending';
                    }
					$lottoHistoryList->result_date =  date('d F Y',strtotime($lottoHistoryList->created_at));  
                }
                $response = ['status' => 'success', 'data' => $lottoHistory];
            }
            else
            {
                $response = ['status' => 'fail', 'data' => 'No data available'];
            }
        }
        else
        {
            $response = ['status' => 'fail', 'data' => 'Values are missing'];
        }
        return response()->json($response);

    }

    public function getTournamentForGuest(){

        $tournaments = Tournament::with(['prizes'])->where('status', '1')->orderBy('start_time','DESC')->get();

        if (count($tournaments)) {
            foreach ($tournaments as $tournament) {

                if (((int)strtotime(date('Y-m-d h:i')) >= (int)strtotime($tournament->start_time)) && ((int)strtotime(date('Y-m-d h:i')) <= (int)strtotime($tournament->end_time))) {

                    $tournament->status = 'running';
                } elseif (((int)strtotime(date('Y-m-d h:i')) >= (int)strtotime($tournament->start_time)) && ((int)strtotime(date('Y-m-d h:i')) >= (int)strtotime($tournament->end_time))) {
                    $tournament->status = 'past';
                } else {
                    $tournament->status = 'upcoming';
                }
            }
            $total_prize = 0;
            if ($tournament->prizes) {

                foreach ($tournament->prizes as $prize) {
                    $total_prize += $prize->prize_amount;
                }
            }
            $tournament->total_prize = $total_prize;
            $response['status'] = 'success';
            $response['total_prize'] = $total_prize;
            $response['data'] = $tournaments;
        } else {
            $response['status'] = 'success';
            $response['data'] = 'No tournament available';
        }
        return response()->json($response);
    }
}