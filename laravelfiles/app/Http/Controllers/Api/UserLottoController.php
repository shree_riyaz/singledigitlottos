<?php

namespace App\Http\Controllers\api;

use App\Model\Lotto;
use App\Model\TournamentUser;
use App\Model\User;
use App\Model\UserLotto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class UserLottoController extends Controller
{
    public function createUserLotto(Request $request)
    {
        date_default_timezone_set("Asia/Kolkata");

        if($request->all() != null)
        {
            $validation = UserLotto::validate($request->all());
            if ($validation->fails()) {
                $response = ['status' => 0, 'error' => $validation->errors(),'data'=>'Errors found.'];
            }else {

                $req = $request->all();
                $cart = json_decode($req['cart']);
                $totalTickets =0;
                foreach($cart as $cartValue)
                {
                    $totalTickets +=   $cartValue->ticket_purchased;
                }
                $request->total_amount = $totalTickets*env("TICKET_PRICE");

                if ($request->is_tournament) {
                    $userTournament = TournamentUser::where('tournament_id',$request->tournament_id)
                        -> where('user_id',Auth::user()->id)
                        ->first();
                    $check_initial_balance = $userTournament->balance;
                }
                else
                {
                    $check_initial_balance = Auth::user()->main_balance;
                }
                if($check_initial_balance>=$request->total_amount) {
                    if((($check_initial_balance/100)*25)>=$request->total_amount) {
                        $lotto_name = $request->lotto_name;
                        $lotto_type = $request->lotto_type;
                        $resultNumber = null;
                        $win = $resultAmount =0;

                        if ($lotto_name == 'binary')
                        {
                            $resultNumber = random_int(0, 1);
                        }
                        elseif ($lotto_name == 'dice')
                        {
                            $resultNumber = random_int(1, 6);
                        }
                        else
                        {
                            $resultNumber = random_int(0, 9);
                        }

                        $lotto_id = Lotto::where('type', $lotto_name)->where('method', $lotto_type)->pluck('id')->first();

                        foreach($cart as $cartValue)
                        {
                            $initial_balance=0;
                            $prize=0;
                            if ($request->is_tournament) {
                                $userTournament = TournamentUser::where('tournament_id',$request->tournament_id)
                                    -> where('user_id',Auth::user()->id)
                                    ->first();
                                $initial_balance = $userTournament->balance;
                            }
                            else {
                                $initial_balance = user::where('id', Auth::user()->id)->pluck('main_balance')->first();
                            }
                            $resultAmount =0;

                            if ($lotto_type == 'instant')
                            {
                                if($cartValue->draw_number==$resultNumber)
                                {
                                    $totalWinningTickets = $cartValue->ticket_purchased;
                                    if ($lotto_name == 'binary')
                                    {
                                        $resultAmount = ($totalWinningTickets * env("BINARY_PRICE"));
                                    }
                                    elseif ($lotto_name == 'dice')
                                    {
                                        $resultAmount = ($totalWinningTickets * env("DICE_PRICE"));
                                    }
                                    else
                                    {
                                        $resultAmount = ($totalWinningTickets * env("DECIMAL_PRICE"));
                                    }
                                }

                            }
                            $ticket_amount = $cartValue->ticket_purchased*env("TICKET_PRICE");
                            $final_balance = $initial_balance - $ticket_amount;
                            $draw_number = $cartValue->draw_number;
                            $ticket_purchased = $cartValue->ticket_purchased;
                            $result = 'pending';
                            $description ='daily lotto ticket amount';
                            if ($lotto_type == 'instant') {
                                if($resultAmount>0)
                                {
                                    $final_balance = $final_balance + $resultAmount;
                                    $result = 'win';
                                    $prize = $resultAmount;
                                    $description ='Instant lotto prize and ticket amount';
                                }
                                else
                                {
                                    $result = 'lost';
                                    $description ='Instant lotto ticket amount';
                                }
                            }
                            $user = User::whereId(Auth::id())->first();
                            if ($request->is_tournament) {
                                $userTournament = TournamentUser::where('tournament_id',$request->tournament_id)->where('user_id',Auth::user()->id)->first();
                                $userTournament->balance = $final_balance;
                                $userTournament->save();

                            } else {
                                $user->main_balance = $final_balance;
                            }
                            $user->save();
                            $method = $request->lotto_type;
                            $details = array_merge($request->all(), array('user_id' => Auth::user()->id, 'initial_balance' => $initial_balance, 'final_balance' => $final_balance, 'lotto_id' => $lotto_id, 'result' => $result, "method" => $method, 'lotto_type' => $request->lotto_name, "draw_number" =>$draw_number, "result_number" => $resultNumber,"total_amount"=>$ticket_amount, "ticket_purchased" => $ticket_purchased,'prize'=>$prize,'description'=>$description));
                            $userLotto = UserLotto::create($details);
                            if ($userLotto != null) {
                                if ($method == 'instant') {
                                    if ($result == 'win') {
                                        $win = 1;
                                        $winData = ['status' => 'success', 'data' => 'You won the game', 'number' => $resultNumber];
                                    } else {
                                        $response = ['status' => 'success', 'data' => 'Sorry, You lost the game.', 'number' => $resultNumber];
                                    }
                                } else {
                                    $response = ['status' => 'success', 'data' => 'User lotto created successfully.'];
                                }
                            } else {
                                $response = ['status' => 'fail', 'data' => 'Something went wrong, Try again.'];
                            }


                        }
                        if( $win==1)
                        {
                            $response = $winData;
                        }


                    }
                    else
                    {
                        $response = ['status' => 'fail', 'data' => 'Ticket amount can not be more than 25% of main amount.'];
                    }
                }
                else
                {
                    $response = ['status' => 'fail', 'data' => 'Ticket amount can not be more than available amount.'];
                }
            }
        }
        else
        {
            $response = ['status' => 'fail', 'data' => 'No data sent'];
        }
        return response()->json($response);
    }
    public function getUserLotto(Request $request)
    {
        $user_id = Auth::user()->id;
        $userlottos = UserLotto::with(['lotto'=>function($query){$query->select('name as lotto_name');}])->where('user_id',$user_id)->select('*', DB::raw("DATE_FORMAT(created_at,'%d %M %Y %h:%i %p') AS game_time"))->orderBy('id','DESC')->get();
        $lottoData =[];
        /*foreach($userlottos as $userlotto)
        {
            $draw_number = json_decode($userlotto->draw_number);
            if(count($draw_number)>0)
            {
                foreach($draw_number as $draw_num)
                {
                    $userlottoData = $userlotto;
                    $userlottoData->draw_number = $draw_num->draw_number;
                    $userlottoData->ticket_purchased = $draw_num->ticket_purchased;
                    $lottoData[] = $userlottoData;
                }
            }
        }*/


        if($userlottos)
        {
            $response['status'] = 'success';
            //  $response['data'] = (object)$lottoData;
            $response['data'] = $userlottos;
        }
        else
        {
            $response['status'] = 'fail';
            $response['data'] = array();
        }
        return response()->json($response);
    }
}
