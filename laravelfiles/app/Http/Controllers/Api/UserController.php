<?php

namespace App\Http\Controllers\Api;

use App\Model\User;
use App\Repositories\UserRepositoryInterface;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
Use App\Model\TournamentUser;
use App\Model\Tournament;
class UserController extends RestfulController
{
    protected $source;


    protected $userRepository;

    /**
     * Constructor
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $model = User::orderby('id', 'desc');

        $keyword = $request->get('q');
        if ($keyword) {
            $model->where('email', 'like', '%' . $keyword . '%');
        }

        $users = $model->with('roles')->paginate($this->getPageSize());

        return $this->response($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->_error(self::RESOURCE_METHOD_NOT_IMPLEMENTED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $user = $this->userRepository->find($id);

        $this->userRepository->prepareUserData($user);
        $result = $user->toArray();
        $result['roles'] = User\Role::getScopes($user->id);
        return $this->response($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request)
    {
//        dd($request->all());
        $user = $request->user();
        if ($request->get('id')) {
            $user = $this->userRepository->find($request->get('id'));
            if (!$user) {
                return $this->_error('User not found');
            }
        }
        $user_id = $request->get('id') ? $request->get('id') : $user->id;

        $validator = Validator::make($request->all(), [
            'firstname' => 'required|regex:/^[\pL\s]+$/u',
            'user_name' => 'required',
            'dob' => 'required|date|before:'.date('Y-m-d'),
            'email' => 'required| email| unique:users,email,' . $user_id,
        ],[
            'firstname.regex' => 'Name should be letters only.',
            'dob.before' => 'DOB should not be future or today date.',

        ]);
//        dd($validator->errors());
        if ($validator->fails()) {
            $error = 'Opps, Something went wrong';
            if (!empty($validator->errors()->getMessages())) {
                foreach ($validator->errors()->getMessages() as $key => $value) {
                    $error = isset($value[0]) ? $value[0] : 'Opps, Something went wrong';
                }
            }
            return response()->json([
                'status' => FALSE,
                'error' => $error,
            ]);
        }

        \DB::beginTransaction();
        try {
            $user->user_name = $request->get('user_name');

            $email = $request->get('email');
            if ($email) {
                $user->email = $email;
            }

            // @TODO: Field: "mobile_number"
            $phone_number = $request->get('phone_number');
            if ($phone_number) {
                $user->phone_number = $phone_number;
            }

            // @TODO: Field: "gender"
            $gender = $request->get('gender');
            if ($gender) {
                $user->gender = $gender;
            }
            // @TODO: Field: "country"
	    $country = $request->get('country');
            if ($country) {
                $user->country = $country;
            }
            // @TODO: Field: "firstname"
            $firstname = $request->get('firstname');
            if ($firstname) {
                $user->firstname = $firstname;
            }
            // @TODO: Field: "lastname"
            $lastname = $request->get('lastname');
            if ($lastname) {
                $user->lastname = $lastname;
            }

            // @TODO: Field: "dob"
            $dob = $request->get('dob');
            if ($dob) {
                $user->dob = $dob;
            }

            $user->save();
            $this->setMessage('User was updated');

            /*if($user->email !== $request->get('email')) {
                $verify_email = User\VerifyEmailChange::createToken($user, $request->get('email'));
                // user notify change email mail
                $verify_email->notify(new VerifyEmailChange($user, $verify_email->token));
                $this->setMessage('We sent you an verification code. Please check your e-mail.');
            } */

            \DB::commit();

//            return $this->response($user);
            return response()->json([
                'status' => true,
                'data' => $user,
            ]);
        } catch (\Exception $ex) {
            \DB::rollback();
            return $this->_critical($ex);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            return $this->_error('Not found');
        }
        try {
            $user->delete();
            return $this->_success('User has been deleted');
        } catch (\Exception $ex) {
            return $this->_critical($ex);
        }
    }


    /**
     * Get my account detail.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function getMyAccount(Request $request, $id = null)
    {
        $userId = is_numeric($id) ? $id : $request->user()->id;
        $user = $this->userRepository->find($userId);

        if (!$user) {
            return $this->_error('Not found');
        }
        $this->userRepository->prepareUserData($user);
//       if(! $rankdata->isEmpty())
//       {
//           $user->main_total_users = count($rankdata);
//           foreach($rankdata as $key => $data)
//           {
//               if($data->id ==$userId)
//               {
//                   $user->tournament_rank =$key+1;
//               }
//           }
//           $rankdata = User::select('id','main_balance')->where('main_balance','>',1000)->orderBy('main_balance','DESC')->get();
//           $user->tournament_total_users = count($rankdata);
//           $user->main_rank =0;
//           foreach($rankdata as $key => $data)
//           {
//               if($data->id ==$userId)
//               {
//                   $user->main_rank =$key+1;
//               }
//           }
//       }
//        dd($id);
        $rankdata = User::select('id','main_balance')->where('main_balance','>',1000)->orderBy('main_balance','DESC')->orderBy('created_at','ASC')->get();
//        dd(count($rankdata));
//        dd($rankdata[0]['id']);
        $userRanks = [];
        $i=1; $count =0;
        foreach($rankdata as $data) {
//            if($data->id==$id) {
            if($data->id == $userId) {
                $userRanks['account'][$count]['total_user'] = $rankdata->count();
                $userRanks['account'][$count]['rank'] = $i;
                $userRanks['account'][$count]['name'] = 'Main account';
            }
            $i++;
        }
//        dd($userRanks);

        if(!count($userRanks)>0)
        {
            $userRanks['account'][$count]['total_user'] = $rankdata->count();
            $userRanks['account'][$count]['rank'] = 0;
            $userRanks['account'][$count]['name'] = 'Main account';
        }

        $rankdata = TournamentUser::with(['tournament','user'])->where('user_id',$userId)->get();
//        dd($rankdata);
        $count =1;
        foreach($rankdata as $udata)
        {
            if(isset($udata->tournament->name))
            {
                $userRanks['account'][$count]['rank'] = $udata->ranking?:0;
                $userRanks['account'][$count]['name'] = 'Tournament - '.$udata->tournament->name?:'N/a';
                $userRanks['account'][$count]['total_user'] =
                    TournamentUser::where('tournament_id',$udata->tournament_id)->count()?:0;
                $count++;
            }
        }
        /*$userData =$user->toArray();
        $user = array_merge($userData,$userRanks);
        return $this->response($user);*/

        $userData =$user->toArray();
        $user = array_merge($userData,$userRanks);
        if (count($user)) {
            $response = ['status' => 'success', 'data' =>$user ];
        } else {
            $response = ['status' => 'fail', 'data' => 'no user found'];
        }
        return response()->json($response);
    }

    /**
     * Enable notifications.
     *
     * @param  Request $request
     * @return Response
     */
    public function enableNotifications(Request $request)
    {
        $user_id = $request->user()->id;
        $status = $request->get('status');
        try {
            $user = $this->userRepository->find($user_id);
            $user->enable_notifications = (int)$status;
            $user->save();

            return $this->setMessage('Updated')
                ->response($user);
        } catch (\Exception $ex) {
            return $this->_critical($ex->getMessage());
        }
    }


}
