<?php

namespace App\Http\Controllers\Api;

use DB;
use Hash;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Model\User\Provider;


class LottoController extends RestfulController
{

    public function validateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'user_name' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $error = 'Opps, Something went wrong';
            if (!empty($validator->errors()->getMessages())) {
                foreach ($validator->errors()->getMessages() as $key => $value) {
                    $error = isset($value[0]) ? $value[0] : 'Opps, Something went wrong';
                }
            }
            return response()->json([
                'status' => FALSE,
                'error' => $error,
            ]);
        }

        $user = User::where('email', $request->email)->first();
        if ($user != null) {
            if ($user) {
                $token = $user->createToken($user->user_name)->accessToken;
                return $response = ['success' => 1, 'data' => $user->toArray(), 'token' => $token];
            } else {
                return $response = ['success' => 0, 'data' => 'No record found'];
            }

        } else {
            return $response = ['success' => 0, 'data' => 'No record found'];
        }

    }
    public function loginUseProvider(Request $request, $provider = 'facebook') {
        // validate provider is valid
        if ($provider != 'facebook' && $provider !='gmail') {
            return $this->_error('Provider must be facebook or instagram');
        }

        // validate input request before create new user.
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'user_name'  => 'required',
            'id'    => 'required'
        ]);

        if ($validator->fails()) {
            $error = 'Opps, Something went wrong';
            if (!empty($validator->errors()->getMessages())) {
                foreach ($validator->errors()->getMessages() as $key => $value) {
                    $error = isset($value[0]) ? $value[0] : 'Opps, Something went wrong';
                }
            }
            return response()->json([
                'status' => FALSE,
                'error' => $error,
            ]);
        }
        $email = $request->get('email');
        //dd($email);
        $provider_user_id = $request->get('id');

        // check if user is exist
        $socialAccount = Provider::whereProvider($provider)
            ->whereProviderUserId($provider_user_id)
            ->first();

        if ($socialAccount) {
            $user = $socialAccount->user;

            // increment logged-in number
            $this->userRepository->incrementLogNum($user->id);

            // creating a token without scopes...
            $token = $user->createToken($user->user_name)->accessToken;
            //$token = $user->createToken(self::TOKEN_NAME)->accessToken;

            $this->userRepository->prepareUserData($user, $provider);
            $user->access_token = $token;

            return $this->response($user);
        } else {
            try{
                $found = $this->userRepository->findByEmail($email);
                if(!$found){
                    $user = new User();
                    $user->firstname = $request->get('firstname');
                    $user->lastname = $request->get('lastname');
                    $user->email = $email;
                    $user->user_name  = $request->get('user_name');
                    $user->last_login  = $request->get('last_login');
                    $user->country  = $request->get('country');
                    $user->timezone  = $request->get('timezone');
                    $user->IP  = $request->get('IP');
                    $user->save();
                }else{
                    $user = $found;
                }

                $socialAccount = new Provider([
                    'user_id' => $user->id,
                    'provider_user_id' => $provider_user_id,
                    'provider' => $provider
                ]);
                $socialAccount->save();

                // increment logged-in number
                $this->userRepository->incrementLogNum($user->id);

                // creating a token without scopes...
                //$this->userRepository->updateCounter($user->id);
                $token = $user->createToken($user->user_name)->accessToken;
                $this->userRepository->prepareUserData($user, $provider);
                $user->access_token = $token;

                return $this->response($user);
            } catch (\Exception $ex) {
                return $this->_critical($ex);
            }
        }
    }
    public function logout(Request $request){
        $validator = Validator::make($request->all(), [
            'device_id'    => 'required',
            'user_id'    => 'required'
        ]);
        if ($validator->fails()) {
            $error = 'Opps, Something went wrong';
            if (!empty($validator->errors()->getMessages())) {
                foreach ($validator->errors()->getMessages() as $key => $value) {
                    $error = isset($value[0]) ? $value[0] : 'Opps, Something went wrong';
                }
            }
            return response()->json([
                'status' => FALSE,
                'error' => $error,
            ]);
        }
        $device_id = $request->get('device_id');
        $user_id   = $request->user()->id;

        try{
            $this->deviceRepository->setDeviceIsInActive($user_id, $device_id);
            return $this->setMessage('success')->response();
        } catch (\Exception $ex) {
            return $this->_error($ex->getMessage());
        }
    }

}
