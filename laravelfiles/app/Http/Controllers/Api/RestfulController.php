<?php
namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Log;

use Carbon\Carbon;

/**** 
HTTP STATUS CODES:
    200 - OK
    201 - Created
    304 - Not Modified
    400 - Bad Request
    401 - Unauthorized
    403 - Forbidden
    404 - Not Found
    500 - Internal Server Error
 ***/

abstract class RestfulController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected $_data       = array();
    protected $_custom     = array();
    protected $_success    = true;
    protected $_message    = null;
    protected $_statusCode = self::HTTP_OK;
    
    /**
     *  Default HTTP statuses
     */
    const HTTP_OK                 = 200;
    const HTTP_CREATED            = 201;
    const HTTP_UNAUTHORIZED       = 403;
    const HTTP_NOT_ACCEPTABLE     = 422;
    const HTTP_INTERNAL_ERROR     = 500;
    
    /**
     *  Default query params
     */
    const QUERY_PARAM_PAGE_NUM    = 'page';
    const QUERY_PARAM_PAGE_SIZE   = 'limit';
    const QUERY_PARAM_ORDER_FIELD = 'order';
    const QUERY_PARAM_ORDER_DIR   = 'dir';
    
    
    /**
     *  Default error messages
     */
    const RESOURCE_NOT_FOUND          = 'Resource not found.';
    const RESOURCE_METHOD_NOT_ALLOWED = 'Resource does not support method.';
    const RESOURCE_METHOD_NOT_IMPLEMENTED    = 'Resource method not implemented yet.';
    const RESOURCE_INTERNAL_ERROR            = 'Resource internal error.';
    const RESOURCE_DATA_PRE_VALIDATION_ERROR = 'Resource data pre-validation error.';
    const RESOURCE_DATA_INVALID  = 'Resource data invalid.';
    const RESOURCE_UNKNOWN_ERROR = 'Resource unknown error.';
    const RESOURCE_REQUEST_DATA_INVALID = 'The request data is invalid.';
    
    /**
     * Collection page sizes
     */
    const PAGE_SIZE_DEFAULT = 10;
    const PAGE_SIZE_MAX     = 100;
    
    /**
     * Execute an action on the controller.
     * 
     * @param  string  $method
     * @param  array   $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        return call_user_func_array([$this, $method], $parameters);
    }
    
    /**
     * Set status code
     * @param  string $code
     * @return self
     */
    protected function setStatusCode($code){
        $this->_statusCode = $code;
        return $this;
    }
    
    /**
     * Set message
     * @param  string $message
     * @return self
     */
    protected function setMessage($message=null){
        $this->_message = $message;
        return $this;
    }
    
    /**
     * Set success
     * @param  string $value
     * @return self
     */
    protected function setSuccess($value){
        $this->_success = (bool)$value;
        return $this;
    }
    
    /**
     * Add custom fields for response
     * @param  array|string $key
     * @param  null|string $value
     * @return string
     */
    protected function addCustom($key=null, $value=null){
        if(is_array($key)){
            foreach ($key as $k => $v){
                $this->_custom[$k] = $v;
            }
        }else{
            $this->_custom[$key] = $value;
        }
        
        return $this;
    }
    
    /**
     * Print success message
     * @param  string|array $message
     * @param  integer $code
     * @return string
     */
    protected function _success($message, $code=self::HTTP_OK){
        if(is_array($message)){
            $this->_data = (array)$message;
            $message     = null;
        }
        
        $this->setMessage($message)
            ->setStatusCode($code);
        
        return $this->response();
    }
    
    /**
     * Print error message
     * @param  string|array $message
     * @param  integer $code
     * @return string
     */
    protected function _error($message, $code=self::HTTP_OK){
        if(is_array($message) || is_object($message)){
            $this->_data = $message;
            $message     = self::RESOURCE_INTERNAL_ERROR;
        }
        
        $this->setSuccess(false)
            ->setMessage($message)
            ->setStatusCode($code);
        
        return $this->response();
    }
    
    /**
     * Print critical message
     * @param  string|Exception $message
     * @param  integer $code
     * @return string
     */
    protected function _critical($message, $code=self::HTTP_INTERNAL_ERROR){
        if($message instanceof \Exception){
            Log::error('Exception: '. $message->getMessage() .' '. $message->getFile() .':'. $message->getLine());
            
            $exception = $message;
            $message   = $message->getMessage();
            
            $this->_data = [
                'line'  => $exception->getLine(),
                'trace' => $exception->getTraceAsString(),
            ];
        }
        
        $this->setSuccess(false)
            ->setMessage($message)
            ->setStatusCode($code);
        
        return $this->response();
    }
    
    /**
     * Response emty object
     * @return JSON string
     */ 
    protected function responseEmtyObject(){
        return $this->response(new \stdClass());
    }
    
    /**
     * Response emty array
     * @return JSON string
     */
    protected function responseEmtyArray(){
        return $this->response();
    }
    
    /**
     * Print JSON response
     * @param  array $data
     * @return string
     */
    protected function response($data=[], $headers=[], $force=false){
        if(!empty($data)){
            $this->_data = $data;
        }
        
        // detect pagination 
        if($this->_data instanceof Paginator 
                || $this->_data instanceof LengthAwarePaginator){
            $this->setPaginator($data);
            
            $pagingArr   = $data->toArray();
            $this->_data = $pagingArr['data'];
        }
        
        // response data
        $errorCode = ($this->_success) ? null : 'err_'. mt_rand();
            $result = array(
            'meta' => array(
                'success' => $this->_success,
                'code'    => $errorCode,
                'message' => $this->_message
            ),
            'data' => $this->_data,
        );
        
        // custom data
        if(is_array($this->_custom)){
            foreach ($this->_custom as $k => $v){
                $result[$k] = $v;
            }
        }
        
        // server timestamp, timezone
        $result['timestamp'] = Carbon::now()->timestamp;
        $result['timezone']  = Carbon::now()->timezoneName;
        
        // turn on output buffering with the gzhandler
        //ob_start('ob_gzhandler');
        
        // force response json
        if(false !== $force){
            http_response_code($this->_statusCode);
            header('Content-Type: application/json');
            echo json_encode($result);
            die();
        }
        
        return Response::json($result, $this->_statusCode);
    }
    protected function responseJson($data=[], $headers=[], $force=false){
        if(!empty($data)){
            $this->_data = $data;
        }

        // detect pagination
        if($this->_data instanceof Paginator
            || $this->_data instanceof LengthAwarePaginator){
            $this->setPaginator($data);
            $pagingArr   = $data->toArray();
            $this->_data = $pagingArr['data'];
        }

        // response data
        $errorCode = ($this->_success) ? null : 'err_'. mt_rand();
        $result = $this->_data;

        // custom data
        if(is_array($this->_custom)){
            foreach ($this->_custom as $k => $v){
                $result[$k] = $v;
            }
        }



        // turn on output buffering with the gzhandler
        //ob_start('ob_gzhandler');

        // force response json
        if(false !== $force){
            http_response_code($this->_statusCode);
            header('Content-Type: application/json');
            echo json_encode($result);
            die();
        }

        return Response::json($result, $this->_statusCode);
    }
    /**
    * Validate request attributes
    * @param   \Illuminate\Http\Request $request
    * @param   array $rules
    * @param   array $messages
    * @param   array $customAttributes
    * @return  object
    */
    public function validate(\Illuminate\Http\Request $request, array $rules, array $messages = [], array $customAttributes = []){
        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);
        
        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();
            
            // base validate error
            $message = null;
            foreach ($errors as $field => $errs){
                $message = isset($errs[0]) ? $errs[0] : null;
                break;
            }
            
            // filtered errors
            $filtered_errors = array();
            foreach ($errors as $field => $errs){
                $filtered_errors[$field] = implode('|', $errs);
            }
            
            return $this->setSuccess(false)
                ->setStatusCode(self::HTTP_OK)
                ->setMessage($message)
                ->response($filtered_errors, [], true);
        }
    }
    
    /**
    * Set paginator data
    * @param   string $paginate
    * @return  object
    */
    protected function setPaginator(LengthAwarePaginator $paginate){
        // init object
        $data = [];
        if(is_object($paginate) && !empty($paginate)){
            $data = $paginate->toArray();
        }
        
        // build vars
        $total        = isset($data['total'])        ? $data['total']        : 0;
        $per_page     = isset($data['per_page'])     ? $data['per_page']     : 0;
        $current_page = isset($data['current_page']) ? $data['current_page'] : 0;
        $last_page    = isset($data['last_page'])    ? $data['last_page']    : 0;
        
        $prev_url = isset($data['prev_page_url']) ? $data['prev_page_url'] : null;
        $next_url = isset($data['next_page_url']) ? $data['next_page_url'] : null;
        $self_url = ($current_page) ? $paginate->url($current_page) : null;
        
        // paging fields
        $this->_custom['paging'] = [
            'total'        => $total,
            'current_page' => $current_page,
            'per_page'     => $per_page,
            'last_page'    => $last_page,
        ];
        
        // links fields
        $this->_custom['links'] = array(
            '_prev' => $prev_url,
            '_self' => $self_url,
            '_next' => $next_url,
        );
        
        return $this;
    }
    
    /**
     * Retrieve page number
     * @return integer|null
     */
    protected function getPageNumber(){
        return Request::get(self::QUERY_PARAM_PAGE_NUM, 1);
    }
    
    /**
     * Retrieve page size
     * @return integer|null
     */
    protected function getPageSize(){
        return Request::get(self::QUERY_PARAM_PAGE_SIZE, self::PAGE_SIZE_DEFAULT);
    }
    
    /**
     * Get sort order direction
     *
     * @return mixed
     */
    protected function getOrderDirection()
    {
        return Request::get(self::QUERY_PARAM_ORDER_DIR);
    }

    /**
     * Get sort order field
     *
     * @return mixed
     */
    protected function getOrderField()
    {
        return Request::get(self::QUERY_PARAM_ORDER_FIELD);
    }
    
    /**
     * Get request object
     * @return object|mixed
     */
    protected function getRequest(){
        return \Illuminate\Support\Facades\Request();
    }
    
    /**
     * Get user object
     * @return object|mixed
     */
    protected function getUser(){
        return \Illuminate\Support\Facades\Request::user();
    }
}
