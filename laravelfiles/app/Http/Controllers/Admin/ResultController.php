<?php

namespace App\Http\Controllers\Admin;

use App\ACME\Admin\AdminHelper;
use App\Model\Lotto;
use App\Model\LottoResult;
use App\Model\Permission;
use App\Model\Role;
use App\Model\RoleUser;
use App\Model\Tournament;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResultController extends AdminHelper
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

  public function dailyResult($type)
  {
       $lottoList = Lotto::with('lotto_result')->where(['type'=>$type,'method'=>'daily'])->get();
        $i =0;
      foreach($lottoList as $list)
      {
          if($list->lotto_result)
          {
              $total_prize = $j =0;
              foreach($list->lotto_result as $result)
              {
                  $lottoList[$i]->lotto_result[$j]->total_prizes = 0;
                  if($result->lotto_users)
                  {
                      $k=0;
                      foreach($result->lotto_users as $users) {
                          if (isset($users->created_at) && isset($result->created_at)){
                              if (date('d-m-y', strtotime($users->created_at)) != date('d-m-y', strtotime($result->created_at))) {
                                  unset($lottoList[$i]->lotto_result[$j]->lotto_users[$k]);
                               // echo '<pre>';  print_r($lottoList[$i]->lotto_result[$j]->lotto_users);exit;
                              }
                              else
                                  {
                                      $total_prize += $lottoList[$i]->lotto_result[$j]->lotto_users[$k]->prize;
                                  }
                      }
                          $k++;
                      }
                  }
                  $lottoList[$i]->lotto_result[$j]->total_prizes = $total_prize;
                  $j++;
              }

          }
          $i++;
      }
      if ($lottoList)
      {
          return view('admin.result.list', compact('lottoList'));
      }
  }
  public function tournamentList()
  {
      $tournamentData =  Tournament::with(['users'])->whereDate('end_time','<',date('y-m-d h:i:s'))->get();
      $tournamentData = $tournamentData;
      return view('admin.result.tournamentgrid', compact('tournamentData'));
  }
  public function dailyResultbyid($type,$id)
    {
       $lottoData =  LottoResult::with(['lotto_users','lotto'])->where('id',$id)->get();
        $lottoData = $lottoData[0];
        return view('admin.result.view', compact('lottoData'));

    }

    public function getTournamentbyid($id)
    {
        $tournamentData = Tournament::with('users')->where('id',$id)->get();
         $tournamentData = $tournamentData[0];
        return view('admin.result.tournamentview', compact('tournamentData'));

    }
}
