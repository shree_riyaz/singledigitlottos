<?php

namespace App\Http\Controllers\Admin;
use App\ACME\Admin\AdminHelper;
use App\Model\TournamentPrize;
use Illuminate\Http\Request;
use App\Model\Tournament;

class TournamentController extends AdminHelper
{

    public function index()
    {
        $allTournaments = Tournament::all();
        return view('admin.tournament.grid', compact('allTournaments'));
    }

    public function create()
    {
        return view('admin.tournament.create');
    }

    public function store(Request $request)
    {
                date_default_timezone_set("Asia/Kolkata");

        $validation = Tournament::validate($request->all());
        // dd($validation->errors());

        if ($validation->fails()) {
            flash('oOps!Something went wrong')->warning();
            return redirect()->back()->withInput()->withErrors($validation->errors());
        }
        $details =$request->except('prize_name', 'min_point','max_point','prize');
       $tournamentId =  Tournament::create($details);
        if($tournamentId)
        {

            if(isset($request->prize_name))
            {
               // print_r($request->all());exit;
                $prizeNames = $request->prize_name;
                $min_points = $request->min_point;
                $max_points = $request->max_point;
                $prize_amount = $request->prize;
                for($i=0; $i<count($request->prize_name);$i++)
                {
                   $prizeData['tournament_id'] = $tournamentId->id;
                   $prizeData['prize_name'] = $prizeNames[$i];
                   $prizeData['min_points'] = $min_points[$i];
                   $prizeData['max_points'] = $max_points[$i];
                   $prizeData['prize_amount'] = $prize_amount[$i];
                    TournamentPrize::create($prizeData);
                }
            }

        }
        flash('Tournament created successfully!')->success();

        return redirect('admin/tournament');
    }

    public function edit($id)
    {
        $tournament = Tournament::find($id);
        $prezeData = TournamentPrize::whereTournamentId($id)->get();
        if($tournament)
        {
            return view('admin.tournament.edit', compact('tournament','prezeData'));
        }
        flash('Sorry! role not found')->warning();

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
                date_default_timezone_set("Asia/Kolkata");

        $validation = Tournament::validate($request->all());

        if ($validation->fails()) {
            flash('oOps!Something went wrong')->warning();
            return redirect()->back()->withInput()->withErrors($validation->errors());
        }
        $details =$request->except('prize_id','prize_name', 'min_point','max_point','prize');
        $tournament =  Tournament::whereId($id)->first();
        $tournament->fill($details)->save();

        if($tournament)
        {
            $tournamentPrize = TournamentPrize::where('tournament_id',$tournament->id)->get();
            if(!$tournamentPrize->isEmpty())
            {
                $prize_ids = $request->prize_id?$request->prize_id:0;
                if($prize_ids)
                {
                    foreach($tournamentPrize as $prize)
                    {
                        if(!in_array($prize->id,$prize_ids))
                        {
                            $delete = TournamentPrize::where('id',$prize->id)->first();
                            $delete->delete();
                        }
                    }
                }
            }
            if(isset($request->prize_id))
            {
                // print_r($request->all());exit;
                $prize_ids = $request->prize_id?$request->prize_id:0;
                $prizeNames = $request->prize_name_ext;
                $min_points = $request->min_point_ext;
                $max_points = $request->max_point_ext;
                $prize_amount = $request->prize_ext;
                for($i=0; $i<count($request->prize_id);$i++)
                {
                    $updatePize = TournamentPrize::where('id',$prize_ids[$i])->first();
                    $updatePize->tournament_id = $tournament->id;
                    $updatePize->prize_name = $prizeNames[$i];
                    $updatePize->min_points = $min_points[$i];
                    $updatePize->max_points = $max_points[$i];
                    $updatePize->prize_amount = $prize_amount[$i];
                    $updatePize->save();
                }
            }

            if(isset($request->prize_name))
            {
                $prizeNames = $request->prize_name;
                $min_points = $request->min_point;
                $max_points = $request->max_point;
                $prize_amount = $request->prize;
                for($i=0; $i<count($request->prize_name);$i++)
                {
                    $prizeData['tournament_id'] = $tournament->id;
                    $prizeData['prize_name'] = $prizeNames[$i];
                    $prizeData['min_points'] = $min_points[$i];
                    $prizeData['max_points'] = $max_points[$i];
                    $prizeData['prize_amount'] = $prize_amount[$i];
                    TournamentPrize::create($prizeData);
                }
            }

        }

        flash('Permission updated successfully!')->success();

        return redirect()->back();
    }

    public function destroy($id)
    {
        $findTournament = Tournament::find($id);
        $result = $findTournament->delete();
        $alert = ['type' => $result ? 1 : 0, 'message' => $result ? 'Tournament has been deleted' : 'Tournament delete failed', 'trId' => $id,];
        return response()->json($alert);
    }
}
