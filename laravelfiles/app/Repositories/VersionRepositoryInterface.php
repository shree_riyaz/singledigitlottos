<?php
namespace App\Repositories;

interface VersionRepositoryInterface{
    
    public function getiOsVersion();
    
    public function getAndroidVersion();
    
}

