<?php
namespace App\Repositories;

interface ActivationRepositoryInterface{
    
    public function activateUser($token);
    
    public function createActivation($user);
    
    public function getActivation($user);
    
    public function getActivationByToken($token);
    
    public function deleteActivation($token);
}

