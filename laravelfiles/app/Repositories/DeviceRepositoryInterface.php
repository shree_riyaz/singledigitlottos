<?php
namespace App\Repositories;

interface DeviceRepositoryInterface{
    
    public function process($user_id, $device_id, $device_token, $device_os=null);
    
    public function setDeviceIsNull($user_id, $device_id);
    
    public function getListDeviceOs($user_id);
    
    public function setDeviceIsInActive($user_id, $device_id);
}

