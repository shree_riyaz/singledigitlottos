<?php
namespace App\Repositories;

interface UserRepositoryInterface{
    
    public function find($id, $columns = array('*'));
    
    public function findByEmail($email);
    
    public function isUsernameExists($username);
    
    public function isMobileNumberExists($phone_number);
    
    public function isEmailExists($email);
    
    public function setPassword($user_id, $password);
    
    public function incrementLogNum($user_id, $value=1);
    
    public function addCounter($user_id);
    
    public function updateCounter($user_id, $column='k_pets', $value=0);
    
    public function resetCounter($user_id, $field='k_pets');
    
    public function getCounter($user_id, $column=null);
    
    public function setIsActivated($userId, $bool=true);
    
    public function setIsBlocked($userId, $bool=true);

    public function prepareUserData(&$_user, $provider=null);
    
    public function delete($id);
    
}

