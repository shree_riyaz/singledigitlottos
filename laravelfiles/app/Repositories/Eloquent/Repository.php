<?php
namespace App\Repositories\Eloquent;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Events\RepositoryEntityCreated;
use App\Repositories\Events\RepositoryEntityUpdated;
use App\Repositories\Events\RepositoryEntityDeleted;
use App\Repositories\Exceptions\RepositoryException;
use App\Repositories\Exceptions\InvalidArgumentException;
use App\Repositories\Exceptions\NotFoundException;

abstract class Repository implements RepositoryInterface {
    
    /**
     * @var
     */
    protected $model;
    
    /**
     * Contructor
     * @param Model $model
     * @throws RepositoryException
     */
    public function __construct(Model $model) {
        $this->model = $model;
    }
    
    /**
     * Get repository model
     *
     * @return Model
     */
    public function getModel(){
        return $this->model;
    }
    
    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*')) {
        return $this->model->get($columns);
    }
    
    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage, $columns = array('*')) {
        return $this->model->paginate($perPage, $columns);
    }
    
    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        $model = $this->model->create($data);
        
        event(new RepositoryEntityCreated($this, $model));
        
        return $model;
    }
    
    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute="id"){
        $model = $this->model->find($id, $attribute);
        if(!$model){
            throw new NotFoundException("Record [{$id}] not found");
        }
        
        $updated = $model->update($data);
        
        event(new RepositoryEntityUpdated($this, $model));
        
        return $updated;
    }
 
    /**
     * @param $id
     * @return mixed
     */
    public function delete($id) {
        $model = $this->model->find($id);
        if(!$model){
            throw new NotFoundException("Record [{$id}] not found");
        }
        
        $originalModel = clone $model;
        event(new RepositoryEntityUpdated($this, $originalModel));
        
        $deleted = $model->delete();
        
        return $deleted;
    }
    
    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*')) {
        return $this->model->find($id, $columns);
    }
 
    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*')) {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    /**
     * @param $where
     * @param array $columns
     * @return mixed
     */
    public function findWhere(array $where, $columns = array('*')) {
        return $this->model->where($where)->first($columns);
    }

    /**
     * @param $field
     * @param $where
     * @param array $columns
     * @return mixed
     */
    public function findWhereIn($field, array $where, $columns = array('*')) {
        return $this->model->whereIn($field, $where)->first($columns);
    }

    /**
     * @param $field
     * @param $where
     * @param array $columns
     * @return mixed
     */
    public function findWhereNotIn($field, array $where, $columns = array('*')) {
        return $this->model->whereNotIn($field, $where)->first($columns);
    }
    
    /**
     * Create raw from a data
     *
     * @param array $data
     * @throws RepositoryException
     */
    public function createRaw(array $data){
        try {
            return $this->model->newInstance()->create($data);
        } catch (QueryException $ex) {
            throw new RepositoryException($ex->getMessage());
        }
    }
    
    /**
     * Update raw by ID
     * 
     * @param int $id
     * @param array $data
     * @throws RepositoryException
     */
    public function updateRaw($id, array $data){
        try {
            $updateable = $this->model->newInstance()->findOrFail($id);
            $updateable->fill($data);
            $updateable->save();
            
            return $updateable;
        }catch (ModelNotFoundException $e){
            throw new NotFoundException($e->getMessage());
        }catch (QueryException $e){
            throw new InvalidArgumentException($e->getMessage());
        }
    }
    
    /**
     * Update or crate a record and return the entity
     *
     * @param array $identifiers columns to search for
     * @param array $data
     * @return mixed
     */
    public function updateOrCreate(array $identifiers, array $data){
        $existing = $this->model->where(array_only($data, $identifiers))->first();
        if ($existing) {
            $existing->update($data);
            return $existing;
        }
        
        return $this->create($data);
    }
    
    /**
     * Count all
     *
     * @return int
     */
    public function count(){
        $q = $this->model->newInstance()->getQuery();
        return $q->count();
    }
    
    /**
     * Bulk delete
     *
     * @param null $ids
     * @return int
     */
    public function bulkDelete(array $ids){
        return $this->model->newInstance()
            ->whereIn($this->model->getKeyName(), $ids)
            ->delete();
    }
    
    /**
     * Get property from findBy magic method
     *
     * @param $method
     * @return mixed
     */
    private function getFindByProperty($method){
        return strtolower(str_replace('findBy', '', $method));
    }
    
    /**
     * Check that if a magic method argument is findable or not
     *
     * @param $method
     * @return bool
     */
    private function isFindable($method){
        if (Str::startsWith($method, 'findBy')){
            return true;
        }
        
        return false;
    }

    /**
     * Add the ability to find by property
     *
     * @param $method
     * @param $args
     * @throws PardisanException
     * @return mixed
     */
    public function __call($method, $args){
        if (method_exists($this, $method)) {
            return call_user_func($method, $args);
        }
        
        if ($this->isFindable($method)){
            $property = $this->getFindByProperty($method);
            return call_user_func_array([$this, 'findBy'], [$property, $args[0]]);
        }
        
        throw new RepositoryException("No method with name: [{$method}] found");
    }
}