<?php
namespace App\Repositories\Eloquent;

use DB;
use Hash;
use Log;
use Carbon\Carbon;
use App\Model\User;
use App\Model\User\Provider;
use App\Model\User\Counter;
use App\Model\MobileDevice;
use App\Repositories\UserRepositoryInterface;

class UserRepository extends Repository implements UserRepositoryInterface{
    
    protected $provider;
    protected $counter;
    protected $device;
    
    /**
     * Contructor
     */
    public function __construct(
        User $model, 
        Provider $provider,
        Counter $counter,
        MobileDevice $device
    ){
        $this->model    = $model;
        $this->provider = $provider;
        $this->counter  = $counter;
        $this->device   = $device;
    }
    
    /**
     * Find by email.
     *
     * @param  string $email
     * @return User|null
     */
    public function findByEmail($email){
        $found = $this->model->whereEmail($email)
            ->whereNotNull('email')
            ->first();
        
        return $found;
    }
    
    /**
     * Is username exists.
     *
     * @param  string  $username
     * @return array
     */
    public function isUsernameExists($username){
        $row = $this->model->where(['username'=>$username])
            ->select(['id'])    
            ->first();
        
        return is_null($row) ? false : true;
    }
    
    /**
     * Is mobile number exists.
     *
     * @param  string  $phone_number
     * @return array
     */
    public function isMobileNumberExists($phone_number){
        $row = $this->model->where(['mobile_number'=>$phone_number])
            ->select(['id'])    
            ->first();
        
        return is_null($row) ? false : true;
    }
    
    /**
     * Is email exists.
     *
     * @param  string $email
     * @return array
     */
    public function isEmailExists($email){
        $row = $this->model->where(['email'=>$email])
            ->whereNull('provider_user_id')
            ->select(['id'])
            ->first();
        
        return is_null($row) ? false : true;
    }
    
    /**
     * Set password attribute.
     *
     * @param  int $user_id
     * @param  string $password
     * @return array
     */
    public function setPassword($user_id, $password){
        $hashPassword = Hash::make($password);
        $affectedRows = $this->model->where([
                'user_id' => $user_id, 
            ])
            ->update([
                'password'   => $hashPassword, 
                'updated_at' => Carbon::now(),
            ]);
        
        return $affectedRows;
    }
    
    /**
     * Increment log num.
     *
     * @param  int $user_id
     * @param  int $value
     * @return array
     */
    public function incrementLogNum($user_id, $value=1){
        $affectedRows = DB::table('users')
            ->where(['id' => $user_id])
            ->increment('no_of_login', (int)$value);
         
        return $affectedRows;
    }
    
    /**
     * Add counter.
     *
     * @param  int $user_id
     * @return array
     */
    public function addCounter($user_id){
        return $this->updateCounter($user_id);
    }
    
    /**
     * Update counter.
     *
     * @param  int $user_id
     * @param  string $column
     * @param  int $value
     * @return array
     */
    public function updateCounter($user_id, $column='k_pets', $value=0){
        $affectedRows = 0;

        try{
            $row = $this->counter->where(['user_id' => $user_id])
                ->first();
            
            if(is_null($row)){
                $this->counter->insert(['user_id' => $user_id]);
            }
            
            if($value > 0){
                $affectedRows = $this->counter->where(['user_id' => $user_id])
                    ->increment($column, (int)$value);
            }else{
                $affectedRows = $this->counter->where(['user_id' => $user_id])
                    ->where($column, '>', 0)
                    ->decrement($column, -(int)$value);
            }
        }  catch (\Exception $ex){
            Log::error($ex->getMessage());
            
            return false;
        }
        
        return $affectedRows;
    }
    
    /**
     * Reset counter.
     *
     * @param  int $user_id
     * @param  string $field
     * @return array
     */
    public function resetCounter($user_id, $field='k_pets'){
        try{
            $row = $this->counter->where(['user_id' => $user_id])
                ->first();

            if(is_null($row)){
                $this->counter->insert(['user_id' => $user_id]);
            }
            
            $affectedRows = $this->counter->where(['user_id' => $user_id])
                ->update([
                    $field => 0
                ]);
            
            return $affectedRows;
        }  catch (\Exception $ex){
            Log::error($ex->getMessage());

            return false;
        }
    }
    
    /**
     * Get counter.
     *
     * @param  int $user_id
     * @param  string $column
     * @return array
     */
    public function getCounter($user_id, $column=null){
        $where = [
            'user_id' => $user_id
        ];
        
        if(null == $column){
            $rows = $this->counter->where($where) 
            ->first();
            
            return $rows;
        }
        
        $row = $this->counter->where($where)
            ->select([
                $column
            ])
            ->first();
        
        return is_null($row) ? 0 : (int)$row->{$column};
    }
    
    /**
     * Set user is activated
     *
     * @param  int  $userId
     * @param  bool  $bool
     * @return int
     */
    public function setIsActivated($userId, $bool=true){
        return $this->model->where([
                $this->model->getKeyName() => $userId
            ])
            ->update([
                'activated' => (int)$bool
            ]);
    }
    
    /**
     * Set user is blocked
     *
     * @param  int  $userId
     * @param  bool  $bool
     * @return int
     */
    public function setIsBlocked($userId, $bool=true){
        return $this->model->where([
                $this->model->getKeyName() => $userId
            ])
            ->update([
                'is_blocked' => (int)$bool
            ]);
    }
    
    /**
     * Repare user data
     *
     * @param  User  $_user
     * @param  string  $provider
     * @return mixed
     */
    public function prepareUserData(&$_user, $provider=null){
        $user_id = ($_user instanceof User) ? $_user->id : $_user;
        $user    = $this->model->find($user_id);

        // provider
        $provider_user_id = null;
        if($provider){
            $socialAccount = $this->provider->whereProvider($provider)
                ->whereUserId($user_id)
                ->select(['provider_user_id', 'provider'])
                ->first();
                
            $provider_user_id = $socialAccount 
                ? $socialAccount->provider_user_id 
                : null;
        }
        $user->provider_user_id = $provider_user_id;
        $user->provider = $provider;
        
        // check first time login
        $user->first_time = ($user->log_num==1) ? 1 : 0;
        $_user = $user;
    }
    
    /**
     * Remove user by ID
     * @param $id
     * @return mixed
     */
    public function delete($id){
        try{
            $this->provider->whereUserId($id)->delete();
            $this->counter->whereUserId($id)->delete();
            $this->device->whereUserId($id)->delete();
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }
        
        return parent::delete($id);
    }
}