<?php

namespace App\Repositories\Eloquent;

use App\Model\MobileDevice;
use Illuminate\Http\Request;
use App\Repositories\DeviceRepositoryInterface;
use Carbon\Carbon;
use DB;
use Log;

class DeviceRepository extends Repository implements DeviceRepositoryInterface
{
    /**
     * Contructor
     */
    public function __construct(MobileDevice $model)
    {
        $this->model = $model;
    }

    /**
     * Process mobile device.
     *
     * @param  int $user_id
     * @param  string $device_id
     * @param  string $device_token
     * @param  int $device_os
     *
     * @return string
     */
    public function process($user_id, $device_id, $device_token, $device_os = null)
    {
        try {
            $where = [
                'user_id' => $user_id,
                'device_id' => strtolower($device_id),
            ];
            $row = $this->model->where($where)
                ->first();

            if (!is_null($row)) {
                $affectedRows = $this->model->where($where)
                    ->update([
                        'device_token' => $device_token,
                        'is_active' => 1,
                        'updated_at' => Carbon::now(),
                    ]);

                return $affectedRows;
            }

            $affectedRows = $this->model->insert([
                'user_id' => $user_id,
                'device_id' => strtolower($device_id),
                'device_token' => $device_token,
                'device_os' => $device_os,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            $lastInsertId = DB::getPdo()->lastInsertId();
            return $lastInsertId;
        } catch (\Exception $ex) {
            Log::error($ex);
            return false;
        }
    }

    /**
     * Set device is null.
     *
     * @param  int $user_id
     * @param  string $device_id
     *
     * @return string
     */
    public function setDeviceIsNull($user_id, $device_id)
    {
        try {
            $affectedRows = $this->model->where([
                'user_id' => $user_id,
                'device_id' => $device_id,
            ])->update([
                'created_at' => null,
                'updated_at' => Carbon::now(),
            ]);

            return $affectedRows;

        } catch (\Exception $ex) {
            Log::error($ex);

            return false;
        }
    }

    /**
     * Get list device os.
     *
     * @param  int $user_id
     *
     * @return string
     */
    public function getListDeviceOs($user_id)
    {
        $results = $this->model->where([
            'user_id' => $user_id,
            'is_active' => 1
        ])
            ->select(['device_os'])
            ->get();

        $device_os_arr = [];
        foreach ($results as $row) {
            $device_os_arr[] = $row->device_os;
        }

        return array_unique($device_os_arr);
    }

    /**
     * Set device is in active.
     *
     * @param  int $user_id
     * @param  string $device_id
     * @return int
     */
    public function setDeviceIsInActive($user_id, $device_id)
    {
        return $this->model->where([
            'user_id' => $user_id,
            'device_id' => $device_id,
        ])->update([
            'is_active' => 0,
        ]);
    }
}

