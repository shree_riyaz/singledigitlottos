<?php
namespace App\Repositories\Eloquent;

use App\Models\Version;
use App\Repositories\VersionRepositoryInterface;

class VersionRepository extends Repository implements VersionRepositoryInterface{
    
    /**
     * Contructor
     */
    public function __construct(Version $model){
        $this->model = $model;
    }
    
    /**
     * Get iOs version
     */
    public function getiOsVersion(){
        return $this->getVersionByOs('ios');
    }
    
    /**
     * Get Android version
     */
    public function getAndroidVersion(){
        return $this->getVersionByOs('android');
    }
    
    /**
     * Get version by OS
     */
    protected function getVersionByOs($os){
        $version = $this->model->whereOs($os)
            ->select(['version', 'force_update'])
            ->first();
        
        return $version;
    }
}

