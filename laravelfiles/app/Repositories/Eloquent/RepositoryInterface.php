<?php
namespace App\Repositories\Eloquent;

/**
 * RepositoryInterface provides the standard functions to be expected of ANY 
 * repository.
 */

interface RepositoryInterface{
    
    public function all($columns = array('*'));

    public function paginate($perPage, $columns = array('*'));

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function find($id, $columns = array('*'));
    
    public function findBy($field, $value, $columns = array('*'));

    public function findWhere(array $where, $columns = array('*'));

    public function findWhereIn($field, array $where, $columns = array('*'));

    public function findWhereNotIn($field, array $where, $columns = array('*'));
    
    public function count();
    
    public function createRaw(array $data);
    
    public function updateRaw($id, array $data);
    
    public function updateOrCreate(array $identifiers, array $data);
    
    public function bulkDelete(array $ids);
}

