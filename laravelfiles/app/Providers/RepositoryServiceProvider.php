<?php 
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider{
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\UserRepositoryInterface',
            'App\Repositories\Eloquent\UserRepository'
        );

        $this->app->bind(
            'App\Repositories\DeviceRepositoryInterface',
            'App\Repositories\Eloquent\DeviceRepository'
        );

        $this->app->bind(
            'App\Repositories\VersionRepositoryInterface',
            'App\Repositories\Eloquent\VersionRepository'
        );
    }
}