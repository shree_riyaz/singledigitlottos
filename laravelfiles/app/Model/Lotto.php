<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lotto extends Model
{
    protected $table    = 'lotto';
    protected $dates    = ['created_at', 'updated_at'];
    public $timestamps  = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','description', 'timezone', 'prizeMoney', 'playingNumber', 'drawTablePrefix'];


    public static function validate($data, $id = null)
    {
        return Validator::make($data, static::rules($id));
    }

    /**
     * Validation Rules
     * @param null $id
     * @return array
     */
    public static function rules($id = null)
    {
        if ($id) {
            return [
                'name' => 'required|string|max:255',
                'description' => 'required|string|max:255',
                'timezone' => 'required',
                'prizeMoney' => 'required|numeric',
                'playingNumber' => 'required|numeric',
            ];
        }

        return [

            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'timezone' => 'required',
            'prizeMoney' => 'required|numeric',
            'playingNumber' => 'required|numeric',
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getIndex()
    {
        return static::orderBy('id', 'desc')->get();
    }

    /**
     * @param $field
     * @param $equalTo
     * @param $value
     * @return mixed
     */
    public static function getUserValue($field, $equalTo, $value)
    {
        return static::where($field,$equalTo)->value($value);
    }

    public function lotto_result()
    {
        return $this->hasMany('App\Model\LottoResult','lottoId')->with('lotto_users');
    }
}