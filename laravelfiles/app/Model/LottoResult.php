<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Lotto;
class LottoResult extends Model
{
    protected $table    = 'lottoresult';
    protected $fillable = ['lottoId', 'result'];

    public function lotto()
    {
        return $this->belongsTo('App\Model\Lotto', 'lottoId');
    }

    public function lotto_users()
    {
            return $this->hasMany('App\Model\UserLotto','lotto_id','lottoId')->with('user')->orderBy('prize','DESC')->orderBy('created_at','ASC');
    }

}
