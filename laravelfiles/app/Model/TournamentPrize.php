<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TournamentPrize extends Model
{
    protected $table    = 'tournament_prize';
    protected $fillable = ['tournament_id', 'prize_name', 'min_points', 'max_points', 'prize_amount'];
}
