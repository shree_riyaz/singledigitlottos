<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserNotifications extends Model
{
    protected $fillable = [
        'user_id','notification_id','seen','notification'
    ];

    public function notification()
    {
        return $this->belongsTo('App\Model\Notifications', 'notification_id');
    }
}
