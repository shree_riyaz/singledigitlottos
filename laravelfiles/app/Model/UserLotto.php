<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Validator;
class UserLotto extends Model
{
    protected $table = 'user_lotto';
    protected $fillable = ['user_id', 'lotto_id', 'draw_number','ticket_purchased', 'total_amount', 'lotto_type', 'is_tournament','initial_balance','final_balance','method','result',"result_number","prize","tournament_id",'description'];
    public static function validate($data, $id = null)
    {
        return Validator::make($data, static::rules($id));
    }

    /**
     * @param null $id
     * @return array
     */
    public static function rules($id = null)
    {
        if ($id) {
            return [
                'lotto_name' => 'required',
                'lotto_type' => 'required',
                'is_tournament' => 'required',
                'cart' => 'required'
                 ];
        }

        return [
            'lotto_name' => 'required',
            'lotto_type' => 'required',
            'is_tournament' => 'required',
            'cart' => 'required',
        ];
    }

    public function lotto()
    {
        return $this->belongsTo('App\Model\Lotto', 'lotto_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id');
    }
    public function tournament()
    {
        return $this->belongsTo('App\Model\Tournament', 'tournament_id');
    }
}
