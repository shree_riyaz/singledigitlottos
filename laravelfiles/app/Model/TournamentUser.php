<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TournamentUser extends Model
{
    protected $table    = 'tournament_user';
    protected $fillable = ['user_id', 'tournament_id', 'ranking', 'prize','balance'];

    public function tournament()
    {
        return $this->belongsTo('App\Model\Tournament', 'tournament_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id');
    }

}
