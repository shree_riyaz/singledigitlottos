<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Validator;

class Tournament extends Model
{

    protected $fillable = ['name', 'description', 'start_time', 'end_time', 'status', 'currency', 'timezone', 'completed'];

    public static function validate($data, $id = null)
    {
        return Validator::make($data, static::rules($id));
    }

    /**
     * @param null $id
     * @return array
     */
    public static function rules($id = null)
    {
        if ($id) {
            return [
                'name' => 'required|string|max:255',
                'start_time' => 'required',
                'end_time' => 'required',
                'currency' => 'required',
                'timezone' => 'required',
            ];
        }
        return [
            'name' => 'required|string|max:255',
            'start_time' => 'required',
            'end_time' => 'required',
            'currency' => 'required',
            'timezone' => 'required',
        ];
    }

    public function prizes()
    {
        return $this->hasMany('App\Model\TournamentPrize', 'tournament_id')->orderBy('min_points', 'DESC')->orderBy('created_at', 'ASC');
    }
    public function users()
    {
        return $this->hasMany('App\Model\TournamentUser', 'tournament_id')->with('user')->orderBy('prize', 'DESC')->orderBy('ranking', 'ASC');
    }

}
