<?php
namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;

use App\Model\User;

class Counter extends Model
{
    protected $table    = 'user_profile_counter';
    protected $fillable = ['user_id', 'k_pets', 'k_posts', 'k_favourites', 'k_following', 
                            'k_receive_notifications', 'k_receive_messages'];
    protected $hidden   = [];
    public $timestamps  = false;
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}