<?php
namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Model\User\Role;
use App\Model\User;

class Permission extends Model
{
    protected $table    = 'permissions';
    protected $fillable = [];
    protected $hidden   = [];
    protected $dates    = ['created_at', 'updated_at'];
    public $timestamps  = true;
    
    /**
     * Permissions can belong to many roles.
     *
     * @return Model
     */
    public function roles(){
        return $this->belongsToMany(Role::class)->withTimestamps();
    }
    
    /**
     * Permissions can belong to many users.
     *
     * @return Model
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_permissions', 'permission_id', 'user_id')->withTimestamps();
    }
    
    /**
     * Get all scopes.
     *
     * @return Model
     */
    public static function getScopes($userId){
        $scopes = DB::table('user_permissions as up')
            ->join('permissions as p', 'p.id', '=', 'up.permission_id')
            ->where([
                'up.user_id' => $userId
            ])
            ->select(['p.id', 'p.name', 'p.slug'])
            ->get();
        
        return $scopes;
    }
}