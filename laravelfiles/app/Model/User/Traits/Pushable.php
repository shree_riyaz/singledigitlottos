<?php
namespace App\Models\User\Traits;

use App\Models\MobileDevice;

trait Pushable
{   
    /**
     * Get the token for Gcm.
     *
     * @param  string  $driver
     * @return mixed
     */
    public function routeNotificationForGcm()
    {
        return $this->getNotificationTokens('gcm');
    }
    
    /**
     * Get the token for Apn.
     *
     * @param  string  $driver
     * @return mixed
     */
    public function routeNotificationForApn()
    {
        return $this->getNotificationTokens('apns');
    }
    
    /**
     * Get mobile device tokens.
     *
     * @param  string  $driver
     * @return array
     */
    protected function getNotificationTokens($driver){
        $device_os = null;
        switch ($driver) {
            case 'gcm':
                $device_os = MobileDevice::ANDROID;
                break;
            case 'apns':
                $device_os = MobileDevice::IOS;
                break;
        }
        
        $tokens = MobileDevice::where([
                'user_id'   => $this->getKey(),
                'device_os' => $device_os,
                'is_active' => 1,
            ])
            ->pluck('device_token')
            ->toArray();
        
        return $tokens;
    }
}