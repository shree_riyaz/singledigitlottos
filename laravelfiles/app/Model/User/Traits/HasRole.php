<?php 
namespace App\Models\User\Traits;

use Illuminate\Support\Facades\Cache;

trait HasRole
{
    /**
     * Get roles.
     * @param string $name
     * @return mixed
     */
    public function getRoles(){
        $roles = Cache::remember(
            'user_roles__'.$this->id,
            60 * 12,
            function () {
                return $this->roles;
            }
        );
        
        return is_null($roles) ? [] : $roles;
    }
    
    /**
     * Has role.
     * @param string $name
     * @return mixed
     */
    public function hasRole($name=null){
        $roles = $this->getRoles();
        
        foreach ($roles as $role){
            if($name == $role->name || $name == $role->slug){
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Attach role.
     *
     * @param int $roleId
     * @return mixed
     */
    public function attachRole($roleId) {
        $this->roles()->attach($roleId);
    }

    /**
     * Dettach role.
     *
     * @param int $roleId
     * @return mixed
     */
    public function detachRole($roleId=null) {
        $this->roles()->detach($roleId);
    }
}