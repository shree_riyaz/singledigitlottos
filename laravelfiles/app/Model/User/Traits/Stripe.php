<?php
namespace App\Models\User\Traits;

use Stripe\Token as StripeToken;

trait Stripe
{   
    /**
     * Create customer's credit card.
     *
     * @param  string  $token
     * @return void
     */
    public function createCard($token){
        if (!$this->hasStripeId()){
            $this->createAsStripeCustomer($token);
            return true;
        }
        
        $tokenObject = StripeToken::retrieve($token, ['api_key' => $this->getStripeKey()]);
        $cards  = $this->cards();
        $exists = false;
        
        if($cards->count() > 0){
            foreach ($cards as $card){
                if($tokenObject->card->fingerprint == $card->fingerprint){
                    $exists = true;
                }
            }
        }
        
        if(!$exists){
            $this->updateCard($token);
        }
        
        return !$exists;
    }
    
    /**
     * Get customer's stripe token.
     *
     * @param  string  $token
     * @return void
     */
    public function getStripeToken($token){
        $object = StripeToken::retrieve($token, ['api_key' => $this->getStripeKey()]);
        
        return $object;
    }
}