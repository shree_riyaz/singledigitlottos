<?php
namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Model\User;
use Illuminate\Notifications\Notifiable;

class VerifyEmailChange extends Model
{
    use Notifiable;
   
    protected $table    = 'user_verify_email_change';
    protected $dates    = ['created_at'];
    public $timestamps  = false;
    protected $fillable = ['user_id'];
    
    private static function getToken(){
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }
    
    public static function createToken($user, $new_email){
        $token = static::getToken();
        $verify_email = self::firstOrNew([
            'user_id' => $user->id,
        ]);

        $verify_email->token = $token;
        $verify_email->email = $new_email;
        $verify_email->created_at = new Carbon();
        $verify_email->save();
        
        return $verify_email;
    }
    
    public static function getVerifyByToken($token){
        return self::where('token', $token)->first();
    }

    public static function changeEmailUser($token) {
        $verify = static::getVerifyByToken($token);
        if(null === $verify){
            return false;
        }

        \DB::beginTransaction();
        try {
            $user = User::find($verify->user_id);
            $user->email = $verify->email;
            $user->save();
            $verify->delete();

            \DB::commit();
            return true;
        }catch (\Exception $ex) {
            \DB::rollback();
            return false;
        }
    }
}