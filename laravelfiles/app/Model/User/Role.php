<?php
namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    protected $table    = 'roles';
    protected $fillable = [];
    protected $hidden   = [];
    protected $dates    = ['created_at', 'updated_at'];
    public $timestamps  = true;
    
    /**
     * Get all scopes.
     *
     * @return Model
     */
    public static function getScopes($userId){
        $scopes = DB::table('user_roles as ur')
            ->join('roles as r', 'r.id', '=', 'ur.role_id')
            ->where([
                'ur.user_id' => $userId
            ])
            ->select(['r.id', 'r.name', 'r.slug'])
            ->orderby('r.id', 'asc')
            ->get();
        
        return $scopes;
    }
}