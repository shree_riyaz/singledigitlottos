<?php
namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Model\User;

class Provider extends Model
{
    protected $table    = 'user_providers';
    protected $fillable = ['user_id', 'provider_user_id', 'provider'];
    protected $hidden   = [];
    protected $dates    = ['created_at', 'updated_at'];
    public $timestamps  = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}