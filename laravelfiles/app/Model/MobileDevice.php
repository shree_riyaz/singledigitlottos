<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MobileDevice extends Model{
    
    protected $table      = 'mobile_devices';
    protected $primaryKey = 'id';
    public    $timestamps = false;
    
    const ANDROID = 1;
    const IOS     = 2;
    
    /**
     * Set the device id.
     *
     * @param  string  $value
     * @return string
     */
    public function setDeviceIdAttribute($value)
    {
        $this->attributes['device_id'] = strtolower($value);
    }
}