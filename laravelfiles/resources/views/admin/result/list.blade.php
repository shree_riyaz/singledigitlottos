@extends('layouts.admin.layout')

@section('title') Users List @stop
@section('css')
    @include('layouts.default.datatable-css')
@stop
@section('content')
    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'User','levelOneLink'=>'/','levelTwo'=>'Grid','levelTwoLink'=>null])

    <div class="row">
        <div class="col-lg-12">
            <table id="datatable-grid" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>Date</th>
                    <th>Draw number</th>
                    <th>Total winners</th>
                    <th>Total prize</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($lottoList as $lotto_result)
                    @if(isset($lotto_result->lotto_result))
                        @php
                            $count=1;
                        @endphp
                    @foreach($lotto_result->lotto_result as $lotto)
                    <tr id="lotto{{$lotto->id}}">
                        <td>{{$count++}}</td>
                        <td>{{date('d M Y h:i A', strtotime("$lotto->created_at"))}}</td>
                        <td>{{$lotto->result}}</td>
                        <td>{{($lotto->lotto_users)?count($lotto->lotto_users):0}}</td>
                        <td>{{$lotto->total_prizes}} LSD</td>
                        <td>
                            <span>
                                <a class="fa fa-eye text-success"
                                   title="View User"
                                   href="{{ url('admin/result/daily/decimal/view').'/'.$lotto->id }}"></a>
                            </span>
                        </td>
                    </tr>
                @endforeach
                    @endif
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection
@section('js')
    @include('layouts.default.datatable-js',['buttons'=>"'copy','csv','excel','pdf','print'",'dom'=>'Bfrtip'])
@stop