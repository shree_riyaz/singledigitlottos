@extends('layouts.admin.layout')

@section('title')
    {{isset($tournamentData->name)?$tournamentData->name:'Lotto'}}
@stop
@section('css')
    @include('layouts.default.datatable-css')
@stop
@section('content')

    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'User','levelOneLink'=>url('admin/user'),'levelTwo'=>'View','levelTwoLink'=>null])

    {{--create sloat and componet for code optimizatrion--}}
     <div class="row">


                            <table class="table table-stripped value-tournament">
                                <tbody><tr>
                                    <td>Tournament Name</td>
                                    <td> {{ucfirst($tournamentData->name)}}</td>
                                </tr>
                                <tr>
                                    <td>Start Date</td>
                                    <td>{{date('d M Y h:i a',strtotime($tournamentData->start_time))}}</td>
                                </tr>
                                <tr>
                                    <td>End Date</td>
                                    <td>  {{date('d M Y h:i a',strtotime($tournamentData->end_time))}}</td>
                                </tr>
                                </tbody>
                            </table>

                        <div class=" col-md-12 col-lg-12 ">
<h1>Winners list</h1>
                            <table id="datatable-grid" class="display nowrap table table-striped"  cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>S.no</th>
                                    <th>Name</th>
                                    <th>Rank</th>
                                    <th>Cash prize</th>
                                    <th>Tournament balance</th>

                                </tr>

                                </thead>
                                <tbody>
                                @if(isset($tournamentData->users))
                                    @php
                                    $sno = 1;
                                    @endphp
                                @foreach($tournamentData->users as $users)
                                    <tr>
                                        <td>{{$sno++}}</td>
                                        <td>
                                            <a href="{{url('/admin/user/'.$users->user->id.'/view')}}">{{$users->user->firstname?$users->user->firstname.'
                                        '.$users->user->lastname: 'N/a'}}</a></td>
                                        <td>{{$users->ranking?$users->ranking:'N/a'}}</td>
                                        <td>{{$users->prize?$users->prize:0}} USD</td>
                                        <td>{{$users->balance?$users->balance:0}} LSD</td>
                                    </tr>
                                @endforeach
                                @endif

                                </tbody>
                            </table>


                        </div>
                    </div>


    <!-- /.col-lg-6 (nested) -->




@endsection
@section('js')
    @include('layouts.default.datatable-js',['buttons'=>"'copy','csv','excel','pdf','print'",'dom'=>'Bfrtip'])
@stop