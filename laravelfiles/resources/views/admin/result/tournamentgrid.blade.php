@extends('layouts.admin.layout')

@section('title') Tournament List @stop
@section('css')
    @include('layouts.default.datatable-css')
@stop
@section('content')
    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'Tournament','levelOneLink'=>'/admin/result/tournament/list','levelTwo'=>'Grid','levelTwoLink'=>null])

    <div class="row">
        <div class="col-lg-12">
            <table id="datatable-grid" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>Tournament Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Total prizes</th>
                    <th>Total users</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $serialNumberCount =1;
                @endphp
                @foreach($tournamentData as $tournament)
                    <tr id="user{{$tournament->id}}">
                        <td>{{$serialNumberCount++}}</td>
                        <td>{{$tournament->name}}</td>
                        <td>{{date('d M Y h:i A', strtotime($tournament->start_time))}}</td>
                        <td>{{date('d M Y h:i A', strtotime($tournament->end_time))}}</td>
                        <td>{{$tournament['users']->sum('prize')}}</td>
                        <td>{{isset($tournament['users'])?count($tournament['users']):0}}</td>
                        <td>
                            <span>
                                <a class="fa fa-eye text-success"
                                   title="View tournament users"
                                   href="{{ url('admin/result/tournament/view').'/'.$tournament->id}}"></a>
                            </span>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
    </div>

@endsection
@section('js')
    @include('layouts.default.datatable-js',['buttons'=>"'copy','csv','excel','pdf','print'",'dom'=>'Bfrtip'])
@stop