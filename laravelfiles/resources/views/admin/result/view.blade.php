@extends('layouts.admin.layout')

@section('title')
    {{isset($lottoData['lotto'])?$lottoData['lotto']->name?$lottoData['lotto']->name:'Lotto':'Lotto'}}
@stop
@section('css')
    @include('layouts.default.datatable-css')
@stop
@section('content')

    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'User','levelOneLink'=>url('admin/user'),'levelTwo'=>'View','levelTwoLink'=>null])

    {{--create sloat and componet for code optimizatrion--}}
     <div class="row">
                        <div class=" col-md-12 col-lg-12 result-title">
                            <h1 class="panel">Result: {{$lottoData->result}} <span  class="dob-result" style="float:right">Date: {{date('d M Y h:i a', strtotime($lottoData->created_at))}}</span></h1>
                            <h2 class="result-shown">Winners</h2><hr>
                            <table id="datatable-grid" class="display nowrap table table-striped"  cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>S.no</th>
                                    <th>Name</th>
                                    <th>Draw number</th>
                                    <th>Ticket purchased </th>
                                    <th>Total amount paid</th>
                                    <th>Result</th>
                                    <th>Prize</th>
                                </tr>

                                </thead>
                                <tbody>
                                @if(isset($lottoData->lotto_users))
                                    @php
                                    $sno = 1;
                                    @endphp
                                @foreach($lottoData->lotto_users as $users)
                                    <tr>
                                        <td>{{$sno++}}</td>
                                        <td>
                                            <a href="{{url('/admin/user/'.$users->user->id?:"".'/view')
                                            }}">{{$users->user->firstname?$users->user->firstname.'
                                        '.($users->user->lastname?:''): 'N/a'}}</a></td>
                                        <td>{{$users->draw_number?:'N/a'}}</td>
                                        <td>{{$users->ticket_purchased?:0}}</td>
                                        <td>{{$users->total_amount?:0}}</td>
                                        <td>{{$users->result?:'N/a'}}</td>
                                        <td>{{$users->prize?:'0'}} LSD</td>
                                    </tr>
                                @endforeach
                                @endif

                                </tbody>
                            </table>


                        </div>
                    </div>


    <!-- /.col-lg-6 (nested) -->




@endsection
@section('js')
    @include('layouts.default.datatable-js',['buttons'=>"'copy','csv','excel','pdf','print'",'dom'=>'Bfrtip'])
@stop