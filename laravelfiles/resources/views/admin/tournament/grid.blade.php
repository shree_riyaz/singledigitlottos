@extends('layouts.admin.layout')

@section('title') Tournament @stop
@section('css')
    @include('layouts.default.datatable-css')
@stop
@section('content')
    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'Tournament','levelOneLink'=>url('admin/tournament'),'levelTwo'=>'Grid','levelTwoLink'=>null])

    <div class="row">
        <div class="col-lg-12 btn-tournament">
            <a class='btn btn-info' href="{{url('admin/tournament/create')}}">Add new Tournament</a>

        </div>
        <div class="col-lg-12">
            <table id="datatable-grid" class="display nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>Name</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Currency</th>
                    <th>Timezone</th>
                    <th>Status</th>
                    <th>Operations</th>
                </tr>
                </thead>
                <tbody>
                @foreach($allTournaments as $tournament)
                    <tr class="blank{{$tournament->id}}">
                        <td>{{$serialNumberCount++}}</td>
                        <td>{{ucfirst($tournament->name)}}</td>
                        <td>{{date('d-M-Y h:i a',strtotime($tournament->start_time))}}</td>
                        <td>{{date('d-M-Y h:i a',strtotime($tournament->end_time))}}</td>
                        <td>{{$tournament->currency}}</td>
                        <td>{{$tournament->timezone}}</td>
                        <td>
                        @if(((int)strtotime(date('Y-m-d h:i')) >= (int)strtotime($tournament->start_time)) && ((int)strtotime(date('Y-m-d h:i')) <= (int)strtotime($tournament->end_time))) {{'Running'}}
                        @elseif(((int)strtotime(date('Y-m-d h:i')) >= (int)strtotime($tournament->start_time)) && ((int)strtotime(date('Y-m-d h:i')) >= (int)strtotime($tournament->end_time))){{'Past'}}
                        @else
                            {{'Upcoming'}}
                            @endif
                        </td>
                        <td>
                            <span>
                                <a class="fa fa-pencil text-primary"
                                   title="Edit"
                                   href="{{ url('admin/tournament').'/'.$tournament->id .'/edit'}}"></a>
                            </span>
                            <span> | </span>
                            <span>
                                <input type='hidden' id='csrf_token' value="{{csrf_token()}}">
                                <a class="fa fa-remove text-danger row-delete"
                                   title="Remove blank"
                                   data-title="blank"
                                   data-href="{{ url('admin/tournament').'/'.$tournament->id .'?_token='.csrf_token() }}">
                                </a>
                            </span>
                            @if(((int)strtotime(date('Y-m-d h:i')) >= (int)strtotime($tournament->start_time)) && ((int)strtotime(date('Y-m-d h:i')) >= (int)strtotime($tournament->end_time)))
                            <span> | </span>
                            <span>
                                <a class="fa fa-trophy"
                                   title="See result"
                                   href="{{ url('/admin/result/tournament/view').'/'.$tournament->id .'?_token='.csrf_token() }}">
                                </a>
                            </span>
                                @endif
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
    </div>

@endsection
@section('js')
    @include('layouts.default.datatable-js')
@stop