@extends('layouts.admin.layout')
@section('title')
    Add Tournament
@stop
@section('content')

    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'Tournament','levelOneLink'=>url('admin/tournament'),'levelTwo'=>'Create','levelTwoLink'=>null])

    {{--create sloat and componet for code optimizatrion--}}
    @component('layouts.admin.partial.panel')
    @slot('panelTitle', 'Tournament')
    @slot('panelBody')

    {!! Form::open(['url'=> 'admin/tournament', 'class' => ''] ) !!}

    @include('admin.tournament.create-edit-common',['submitButtonName'=>'save'])

    {!! Form::close() !!}

    @endslot
    @endcomponent

@section('js')
    @include('layouts.default.tournament-js')
@stop
@endsection

