<div class="col-lg-6">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter name']) !!}
        @if($errors->has('name'))
            <span class="text-danger" >  {{ $errors->first('name') }}   </span>
        @endif
    </div>
</div>

<div class="col-lg-6">
    <div class="form-group{{ $errors->has('currency') ? ' has-error' : '' }}">
        {!! Form::label('currency', 'Currency', ['class' => 'control-label']) !!}

        {{ Form::select('currency', ['USD'=>'USD','LSD'=>'LSD'], null, ['class' => 'form-control']) }}
        @if($errors->has('currency'))
            <span class="text-danger">  {{ $errors->first('currency') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}">
        {!! Form::label('start_time', 'Start Time', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!! Form::text('start_time', null, ['class' => 'form-control', 'placeholder' => 'Enter start time']) !!}
        @if($errors->has('start_time'))
            <span class="text-danger" >  {{ $errors->first('start_time') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group{{ $errors->has('end_time') ? ' has-error' : '' }}">
        {!! Form::label('end_time', 'End Time', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!! Form::text('end_time', null, ['class' => 'form-control', 'placeholder' => 'Enter end time']) !!}
        @if($errors->has('end_time'))
            <span class="text-danger" >  {{ $errors->first('end_time') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group{{ $errors->has('timezone') ? ' has-error' : '' }}">
        {!! Form::label('timezone', 'Timezone', ['class' => 'control-label']) !!}

        {{ Form::select('timezone', ['IST'=>'IST','GST'=>'GST'], null, ['class' => 'form-control']) }}
        @if($errors->has('timezone'))
            <span class="text-danger">  {{ $errors->first('timezone') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
        {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}

        {{ Form::select('status', [0=>'Disable',1=>'Enable'], null, ['class' => 'form-control']) }}
        @if($errors->has('status'))
            <span class="text-danger">  {{ $errors->first('status') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-12">
    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!!  Form::textArea('description', null, ['class' => 'form-control', 'placeholder' => 'Enter description']) !!}
        @if($errors->has('description'))
            <span class="text-danger" >  {{ $errors->first('description') }}   </span>
        @endif
    </div>
</div>

<!-- /.col-lg-6 (nested) -->
<div class="col-lg-12 prizeSection">
    <table class="prizeTable table table-bordered">

        @if(isset($prezeData))
            @foreach($prezeData as $data)
                <tr>
                <tr><td><label class='control-label'>Name</label><input type='hidden' name='prize_id[]'  value="{{$data->id}}"><input type='text' name='prize_name_ext[]' required  value="{{$data->prize_name}}" class='prize_name form-control'></td><td><label class='control-label'>Min point</label><input required type='text' value="{{$data->min_points}}" name='min_point_ext[]' class='min_point form-control'></td><td><label class='control-label'>Max point</label><input type='text' required value="{{$data->max_points}}" name='max_point_ext[]' class='max_point form-control'></td><td><label class='control-label'>Prize</label><input type='text' required value="{{$data->prize_amount}}" name='prize_ext[]' class='prize form-control'></td><td><button type='button' class='rmvtr btn btn-danger'>Remove</button></td></tr>
                </tr>
            @endforeach
            @else
            <tr class="nulltr">
                <td> No prize added.</td>
            </tr>
        @endif
    </table>

    <button id="add_prize" class="btn btn-info" type="button">Add Prize</button>


</div>

<div class="col-lg-12">
    <br>
    <button type="submit" class="btn btn-sm btn-success">{{$submitButtonName}}</button>
    {!! Form::reset('Reset', ['class' => 'btn btn-sm btn-warning']) !!}
    <a href="{{url('admin/tournament')}}" class="btn btn-sm btn-warning">Cancel</a>
</div>
