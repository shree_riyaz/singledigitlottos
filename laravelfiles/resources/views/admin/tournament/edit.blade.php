@extends('layouts.admin.layout')

@section('title')
    Edit tournament
@stop
@section('content')

    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'Role','levelOneLink'=>url('admin/tournament'),'levelTwo'=>'Edit','levelTwoLink'=>null])

    {{--create sloat and componet for code optimizatrion--}}
    @component('layouts.admin.partial.panel')
    @slot('panelTitle', 'Edit tournament')
    @slot('panelBody')

    {{ Form::model($tournament, array('url' => array('admin/tournament', $tournament->id),'method'=>'PUT')) }}

    @include('admin.tournament.create-edit-common',['submitButtonName'=>'Update'])

    {!! Form::close() !!}

    @endslot
    @endcomponent
@section('js')
    @include('layouts.default.tournament-js')
@stop
@endsection
