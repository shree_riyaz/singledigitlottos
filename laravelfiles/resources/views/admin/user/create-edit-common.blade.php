<div class="col-lg-12">
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
            {!! Form::label('name', 'First name', ['class' => 'control-label ']) !!}
            <span class="text-danger">*</span>

            {!! Form::text('firstname',null , ['class' => 'form-control', 'placeholder' => 'Enter first name']) !!}
            @if($errors->has('firstname'))
                <span class="text-danger">  {{ $errors->first('firstname') }}   </span>
            @endif
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
            {!! Form::label('lastname', 'Last name', ['class' => 'control-label ']) !!}
            <span class="text-danger">*</span>

            {!! Form::text('lastname',null , ['class' => 'form-control', 'placeholder' => 'Enter last name']) !!}
            @if($errors->has('lastname'))
                <span class="text-danger">  {{ $errors->first('lastname') }}   </span>
            @endif
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
            {!! Form::label('phone_number', 'Phone No.', ['class' => 'control-label ']) !!}
            <span class="text-danger">*</span>

            {!! Form::text('phone_number', null, ['class' => 'form-control', 'placeholder' => 'Enter name','maxlength'=>12]) !!}
            @if($errors->has('phone_number'))
                <span class="text-danger">{{ $errors->first('phone_number') }}  </span>
            @endif
        </div>

    </div>
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            {!! Form::label('email', 'Email', ['class' => 'control-label ']) !!}
            <span class="text-danger">*</span>

            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter email','maxlength'=>12]) !!}
            @if($errors->has('email'))
                <span class="text-danger">  {{ $errors->first('phone_number') }}   </span>
            @endif
        </div>

    </div>
</div>
<!-- /.col-lg-6 (nested) -->
