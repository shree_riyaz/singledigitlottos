@extends('layouts.admin.layout')

@section('title')
    {{$userList->firstname?$userList->firstname.' '.$userList->lastname:'User details'}}
@stop
@section('content')

    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'User','levelOneLink'=>url('admin/user'),'levelTwo'=>'View','levelTwoLink'=>null])

    {{--create sloat and componet for code optimizatrion--}}




    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 toppad" >
sdfdsfsdfd

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$userList->firstname?$userList->firstname.' '.$userList->lastname:'N/a'}}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">


                        <div class=" col-md-12 col-lg-12 ">
                            <table class="table table-user-information">
                                <tbody>

                                <tr>
                                    <td>Date of Birth</td>
                                    <td>{{(date('d M Y', strtotime($userList->dob))=='01 Jan 1970')?'Not added':date('d M Y', strtotime($userList->dob))}}</td>
                                </tr>

                                <tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>{{($userList->gender=='f')?'Female':'Male'}}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><a href="mailto:{{$userList->email}}">{{$userList->email}}</a></td>
                                </tr>
                                <tr>
                                <td>Phone Number</td>
                                <td>{{$userList->phone_number?$userList->phone_number:'N/a'}}</td>

                                </tr>
                                <tr>
                                <td>Total login</td>
                                <td>{{$userList->no_of_login?$userList->no_of_login:'0'}} times</td>

                                </tr>
                                <tr>
                                <td>Country</td>
                                <td>{{$userList->country?$userList->country:'N/a'}} </td>
                                </tr>
                                </tr>
                                <tr>
                                <td>Account created on</td>
                                <td>{{$userList->created_at?date('d M Y', strtotime($userList->created_at)):'N/a'}}</td>
                                </tr>
                                <tr>
                                <td>Total lotto played</td>
                                <td>{{isset($userList['lottoGame'])?count($userList['lottoGame']->toArray()):0}} times</td>

                                </tr>
                                <tr>
                                <td>Total tournament played</td>
                                <td>{{isset($userList['lottoTournament'])?count($userList['lottoTournament']->toArray()):0}} times</td>

                                </tr>

                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <h4 class="btn btn-sm btn-primary">Main Balance: {{$userList->main_balance?$userList->main_balance:0}} LD</h4>
                    <h4  class="btn btn-sm btn-warning">Cash Balance: {{$userList->cash_prize?$userList->cash_prize:0}} USD</h4>

                    <div class="back-btn" style="float: right; display: inline-block;">
                        <h4><a href="{{url('/admin/user')}}" class="btn btn-sm btn-primary">Back</a></h4>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- /.col-lg-6 (nested) -->




@endsection
