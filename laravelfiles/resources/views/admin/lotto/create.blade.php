@extends('layouts.admin.layout')
@section('title')
    Add Lotto
@stop
@section('content')

    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'blank page','levelOneLink'=>url('admin/lotto'),'levelTwo'=>'Create','levelTwoLink'=>null])

    {{--create sloat and componet for code optimizatrion--}}
    @component('layouts.admin.partial.panel')
    @slot('panelTitle', 'Add Lotto')
    @slot('panelBody')

        {!! Form::open(['url'=> 'admin/lotto', 'class' => ''] ) !!}

             @include('admin.lotto.create-edit-common',['submitButtonName'=>'save'])

        {!! Form::close() !!}

    @endslot
    @endcomponent


@endsection
