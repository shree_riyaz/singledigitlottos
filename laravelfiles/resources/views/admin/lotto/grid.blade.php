@extends('layouts.admin.layout')

@section('title') Lotto @stop
@section('css')
    @include('layouts.default.datatable-css')
@stop
@section('content')
    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'Lotto','levelOneLink'=>url('admin/lotto'),'levelTwo'=>'Grid','levelTwoLink'=>null])

    <div class="row">
        <div class="col-lg-12">
            <table id="datatable-grid" class="display nowrap lotto-table" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>{{trans('name')}}</th>
                    {{--<th>{{trans('Description')}}</th>--}}
                    <th>{{trans('Timezone')}}</th>
                    <th>{{trans('Price Points')}}</th>
                    <th>{{trans('Number Prefix')}}</th>
                    <th>{{trans('Points')}}</th>
                    <th>{{trans('Created Date')}}</th>
                    <th>{{trans('Operations')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($allLottos as $lotto)
                    <tr>
                        <td>{{$serialNumberCount++}}</td>
                        <td>{{$lotto->name}}</td>
                        {{--<td>{{$lotto->description}}</td>--}}
                        <td>{{$lotto->timezone}}</td>
                        <td>{{$lotto->prizeMoney}}</td>
                        <td>{{$lotto->drawNumberPrefix}}</td>
                        <td>{{$lotto->price}}</td>
                        <td>{{$lotto->created_at}}</td>
                        <td>
                            <span>
                                <a class="fa fa-pencil text-primary"
                                   title="Edit"
                                   href="{{ url('admin/lotto').'/'.$lotto->id .'/edit'}}"></a>
                            </span>
                            <span> | </span>
                            <span>
                                <a class="fa fa-remove text-danger row-delete"
                                   title="Remove lotto"
                                   data-title="lotto"
                                   data-href="{{ url('admin/lotto').'/'.$lotto->id .'?_token='.csrf_token() }}">
                                </a>
                            </span>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
    </div>

@endsection
@section('js')
    @include('layouts.default.datatable-js',['buttons'=>"'copy','csv','excel','pdf','print'",'dom'=>'Bfrtip'])
@stop