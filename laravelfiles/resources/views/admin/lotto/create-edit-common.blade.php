<div class="col-lg-6">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Name']) !!}
        @if($errors->has('name'))
            <span class="text-danger" >  {{ $errors->first('name') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Enter Description']) !!}
        @if($errors->has('description'))
            <span class="text-danger" >  {{ $errors->first('description') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group{{ $errors->has('timezone') ? ' has-error' : '' }}">
        {!! Form::label('timezone', 'Timezone', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!! Form::text('timezone', null, ['class' => 'form-control', 'placeholder' => 'Enter Timezone']) !!}
        @if($errors->has('timezone'))
            <span class="text-danger" >  {{ $errors->first('timezone') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group{{ $errors->has('timezone') ? ' has-error' : '' }}">
        {!! Form::label('timezone', 'Timezone', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!! Form::text('timezone', null, ['class' => 'form-control', 'placeholder' => 'Enter Timezone']) !!}
        @if($errors->has('timezone'))
            <span class="text-danger" >  {{ $errors->first('timezone') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group{{ $errors->has('prizeMoney') ? ' has-error' : '' }}">
        {!! Form::label('prizeMoney', 'Price Points', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!! Form::text('prizeMoney', null, ['class' => 'form-control', 'placeholder' => 'Enter Timezone']) !!}
        @if($errors->has('prizeMoney'))
            <span class="text-danger" >  {{ $errors->first('prizeMoney') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group{{ $errors->has('playingNumbers') ? ' has-error' : '' }}">
        {!! Form::label('playingNumbers', 'Playing Numbers', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!! Form::text('playingNumbers', null, ['class' => 'form-control', 'placeholder' => 'Select Numbers']) !!}
        @if($errors->has('playingNumbers'))
            <span class="text-danger" >  {{ $errors->first('playingNumbers') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
        {!! Form::label('price', 'Points', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Enter Price']) !!}
        @if($errors->has('price'))
            <span class="text-danger" >  {{ $errors->first('price') }}   </span>
        @endif
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group{{ $errors->has('drawNumberPrefix') ? ' has-error' : '' }}">
        {!! Form::label('drawNumberPrefix', 'Prefix', ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>

        {!! Form::text('drawNumberPrefix', null, ['class' => 'form-control', 'placeholder' => 'Enter Prefix']) !!}
        @if($errors->has('drawNumberPrefix'))
            <span class="text-danger" >  {{ $errors->first('drawNumberPrefix') }}   </span>
        @endif
    </div>
</div>
{{--<div class="col-lg-6">
    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
        {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}

        {{ Form::select('status', [0=>'Disable',1=>'Enable'], null, ['class' => 'form-control']) }}
        @if($errors->has('status'))
            <span class="text-danger">  {{ $errors->first('status') }}   </span>
        @endif
    </div>--}}
</div>
<!-- /.col-lg-6 (nested) -->

<div class="col-lg-12">
    <button type="submit" class="btn btn-sm btn-success">{{$submitButtonName}}</button>
    {!! Form::reset('Reset', ['class' => 'btn btn-sm btn-warning']) !!}
    <a href="{{url('admin/blank')}}" class="btn btn-sm btn-warning">Cancel</a>
</div>
